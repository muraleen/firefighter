function [  ] = plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds)

    % PLOTS
    figure(1);
    clf;
    subplot(1,2,1); % ARENA | SIMULATION VISUALIZATION
    hold on;

    % Draw Doorway white tape
    for tape = 1:size(tapes,1)
        plot(tapes(tape,[1 3]), tapes(tape,[2 4]), 'Color', [0.7 0.7 0.7] ,'LineStyle', '--');
    end
    
    % Draw Arena Walls
    for wall = 1:size(walls,1)
        plot(walls(wall,[1 3]), walls(wall,[2 4]), 'Color', [0 0 0] ,'LineStyle', '-', 'LineWidth', 3);
    end
    
    % Draw Robot Position and Heading
    th = 0:360;
    plot(RobotPos(1) + (D/2).*cosd(th), RobotPos(2) + (D/2).*sind(th), 'b'); % Plot Rover
    plot([RobotPos(1) RobotPos(1)+((D/2).*cosd(RobotHdg))], [RobotPos(2) RobotPos(2)+((D/2).*sind(RobotHdg))], 'b-');
    
    % Draw Candle Position
    plot(CandlePos(1), CandlePos(2), 'ro');
    
    hold off;
    grid on;
    axis(bounds);
    axis square;
    
    subplot(1,2,2); % LiDAR | VISIBLE RANGE MAP

    polar((LiDARMap(:,1)).*(pi/180),LiDARMap(:,2),'r.');
    
    grid on;
    axis equal;

end

