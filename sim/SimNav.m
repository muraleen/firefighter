clear all;
close all;
clc;

% Hardware Specifications
LiDARRange = [20 600]; % [cm] LiDAR Range
LiDARdtheta = 1; % [deg] LiDAR Angular Resolution
LiDARdr = 0.2; % [cm] LiDAR Linear Resolution

map = [];

% Simulation Parameters
run = 1; % run simulation flag
tth = 1; % [cm] Thickness tolerance threshold
dt = 0.1; % [s] simulation time step
wptTol = 20;
bounds = [0 244 0 244]; % [cm] Maze bounds (MATLAB axis)
RobotPos = [25 170]; % [cm] Position of Robot (x,y)
RobotHdg = 270; % [deg] Heading of Robot, with 0 deg pointing E, CCW +
LiDARMap = [transpose(0:359), zeros(360,1)+600];
turnDeg = 6;
moveCM = 2;

reached = 0;

tgtNode = 13;

% Define Arena Walls
walls = [  0   0   0 244; % W Boundary
           0   0 244   0; % S Boundary
         244   0 244 244; % E Boundary
           0 244 244 244; % N Boundary
         126   0 126  45; % Room 1 W Wall    [M]
         126  91 198  91; % Room 1 N Wall
           0 103  72 103; % Room 2 N Wall
          72  56  72 103; % Room 2 E Wall
          46 157  72 157; % Room 3 S Wall
          72 157  72 244; % Room 3 E Wall
         126 147 126 198; % Room 4 W Wall
         126 147 196 147; % Room 4 S Wall    [M]
         196 147 196 198; % Room 4 E Wall
         126 198 150 198]; % Room 4 N Wall   [M]
     
% Preferred pathways
nodes = [  20  155; % N0
           20  124; % N1
           100 124; % N2
           100 220; % N3
           172 220; % N4
           172 198; % N5
           220 220; % N6
           220 124; % N7
           220 90;  % N8
           172 124; % N9
           172 148; % N10
           100 25;  % N11
           72  25;  % N12
           125 25;  % N13
           100 70;  % N14
           125 70]; % N15
      
paths = [ 0 1;
          1 2;
          2 3;
          3 4;
          4 5;
          4 6;
          6 7;
          7 8;
          7 9;
          9 10;
          2 9;
          2 14;
          14 15;
          11 14;
          11 12;
          11 13];
      
for path = 1:size(paths,1)
    paths(path,1) = paths(path,1) + 1;
    paths(path,2) = paths(path,2) + 1;
end
     
while run
    
    % clc;
    
    LiDARMap = [transpose(0:359), zeros(360,1)+600];
    
    % LiDAR Generate Range Map
    for theta = 0:359
        % Line12 - line from robot to LiDAR range along theta
        x1 = RobotPos(1);
        y1 = RobotPos(2);
        x2 = RobotPos(1) + LiDARRange(2)*cosd(RobotHdg + theta);
        y2 = RobotPos(2) + LiDARRange(2)*sind(RobotHdg + theta);
        
        for wall = 1:size(walls,1)            
            % Line34 - wall
            x3 = walls(wall,1);
            y3 = walls(wall,2);
            x4 = walls(wall,3);
            y4 = walls(wall,4);
            
            % Find Intersection point
            den = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
            numTerm1 = x1*y2 - y1*x2;
            numTerm2 = x3*y4 - y3*x4;
            Px = (numTerm1*(x3 - x4) - numTerm2*(x1 - x2))/den;
            Py = (numTerm1*(y3 - y4) - numTerm2*(y1 - y2))/den;
            
            % Check if intersection point is between bounds
            if ((Px >= x1-tth && Px <= x2+tth) || (Px >= x2-tth && Px <= x1+tth)) && ...
               ((Py >= y1-tth && Py <= y2+tth) || (Py >= y2-tth && Py <= y1+tth)) && ...
               ((Px >= x3-tth && Px <= x4+tth) || (Px >= x4-tth && Px <= x3+tth)) && ...
               ((Py >= y3-tth && Py <= y4+tth) || (Py >= y4-tth && Py <= y3+tth)) ;
                theta_corr = theta;
                if theta > 359
                    theta_corr = theta - 360;
                end
                d = round(sqrt((Px - RobotPos(1))^2 + (Py - RobotPos(2))^2)*5)/5;
                if d < LiDARMap(theta_corr+1,2)
%                     if d < LiDARRange(1)
%                         d = LiDARRange(1);
%                     end
                    LiDARMap(theta_corr+1,2) = d;
                end
            end
            
        end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%% NAVIGATION ALGORITHM %%%%%%%%%%%%%%%%%%%%%%%%%%

    if ~reached
        possiblePaths = {};
        minD2N = 244;
        nearestNode = 0;
        for node = 1:size(nodes,1)
            d2N = norm(nodes(node,:) - RobotPos);
            if d2N < minD2N
                minD2N = d2N;
                nearestNode = node;
            end
        end

        if nearestNode ~= tgtNode
            possiblePaths{1} = [0 nearestNode];
            reached = 0;
            while ~reached % Iterative process
                for n = 1:size(possiblePaths,2) % For each possible path
                    pathway = possiblePaths{n}; % Just to make this a little easier to read
                    % Check all possible next nodes and append to pathway vector
                    found = 0;
                    for path = 1:size(paths,1)
                        if paths(path,1)==pathway(size(pathway,2)) && paths(path,2)~=pathway(size(pathway,2)-1)
                            if paths(path,2) == tgtNode
                                reached = 1;
                                finalPath = [pathway tgtNode];
                            else
                                % NEXT NODE: paths(path,2)
                                if ~found % Append node
                                    possiblePaths{1,n} = [pathway paths(path,2)];
                                    found = 1;
                                else % Fork pathway
                                    possiblePaths{1,size(possiblePaths,2)+1} = [pathway paths(path,2)];
                                end
                            end
                        elseif paths(path,2)==pathway(size(pathway,2)) && paths(path,1)~=pathway(size(pathway,2)-1)
                            if paths(path,1) == tgtNode
                                reached = 1;
                                finalPath = [pathway tgtNode];
                            else
                                % NEXT NODE: paths(path,1)
                                if ~found % Append node
                                    possiblePaths{1,n} = [pathway paths(path,1)];
                                    found = 1;
                                else % Fork pathway
                                    possiblePaths{1,size(possiblePaths,2)+1} = [pathway paths(path,1)];
                                end
                            end
                        end
                    end
                end
            end
        else
            finalPath = [0 tgtNode];
        end
        
    end
    
    % Move to the target node!
    
    % WAYPOINT TRANSITION
    if norm(RobotPos - nodes(finalPath(1,2),:)) <= wptTol
        if finalPath(1,2) == tgtNode
            disp('Target Node Reached!');
            % RobotPos = nodes(tgtNode,:);
            
            % tgtNode = tgtNode + 1;
            reached = 1;
            
            break;
        else
            finalPath = [0 finalPath(1,3:size(finalPath,2))];
        end
    end
    
    % TURN TO TARGET HEADING
    tgtPath = nodes(finalPath(1,2),:) - RobotPos;
    tgtHdg = atan2(tgtPath(2),tgtPath(1))*(180/pi);
    if tgtHdg < 0
        tgtHdg = tgtHdg + 360
    end
    if RobotHdg < 0
        RobotHdg = RobotHdg + 360;
    elseif RobotHdg > 360
        RobotHdg = RobotHdg - 360;
    end
    
    if tgtHdg - RobotHdg > 180
        tgtHdg = tgtHdg - 360;
    elseif RobotHdg - tgtHdg > 180
        tgtHdg = tgtHdg + 360;
    end
    
    if tgtHdg > RobotHdg + turnDeg
        RobotHdg = RobotHdg + turnDeg;
    elseif tgtHdg < RobotHdg - turnDeg
        RobotHdg = RobotHdg - turnDeg;
    else
        RobotHdg = tgtHdg;
    end

    % MOVE FORWARD
    RobotPos = RobotPos + moveCM.*[cosd(RobotHdg) sind(RobotHdg)];
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % PLOTS
    figure(1);
    clf;
    subplot(1,2,1); % ARENA | SIMULATION VISUALIZATION
    hold on;

    % Draw Area Walls
    for wall = 1:size(walls,1)
        plot(walls(wall,[1 3]), walls(wall,[2 4]), 'Color', [0 0 0] ,'LineStyle', '-', 'LineWidth', 3);
    end
    
    % Draw Preferred pathway nodes
    for node = 1:size(nodes,1)
        if tgtNode == node
            plot(nodes(node,1), nodes(node,2), 'gs');
        else
            plot(nodes(node,1), nodes(node,2), 'ys');
        end
        text(nodes(node,1)+5, nodes(node,2)-5,['N' num2str(node-1)]);
    end
    
    for path = 1:size(paths,1)
        plot([nodes(paths(path,1),1) nodes(paths(path,2),1)], [nodes(paths(path,1),2) nodes(paths(path,2),2)], 'y--');
    end
    
    % PLOT FINAL PATH PLAN
    plot([RobotPos(1) nodes(finalPath(2),1)], [RobotPos(2) nodes(finalPath(2),2)], 'r-');
    if size(finalPath,2) > 2
        for n = 3:size(finalPath,2)
            plot([nodes(finalPath(n-1),1) nodes(finalPath(n),1)], [nodes(finalPath(n-1),2) nodes(finalPath(n),2)], 'r-');
            plot(nodes(finalPath(n-1),1), nodes(finalPath(n-1),2), 'rs');
        end
    end
    
    % Draw Robot Position and Heading    
    plot(RobotPos(1), RobotPos(2), 'bo'); % Plot Rover
    plot([RobotPos(1) RobotPos(1)+(10.*cosd(RobotHdg))], [RobotPos(2) RobotPos(2)+(10.*sind(RobotHdg))], 'b-');
    
    hold off;
    grid on;
    axis(bounds);
    axis square;
    
    subplot(1,2,2); % LiDAR | VISIBLE RANGE MAP

    polar((LiDARMap(:,1) + 90).*(pi/180),LiDARMap(:,2),'r.');
    
    grid on;
    axis equal;
    
    % AUTOMATIC CONTROL

    pause(dt);    
    
    % run = 0;
    
    if ((RobotPos(1) < 0) || (RobotPos(1) > 244) || (RobotPos(2) < 0) || (RobotPos(2) > 244))
        run = 0;
    end

end