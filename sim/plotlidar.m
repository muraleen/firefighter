function [ data ] = plotlidar( file )

    fid = fopen(file);
    data = transpose(fscanf(fid, '%d,%d\n', [2, inf]));
    polar(-data(:,1)*(pi/180),data(:,2),'k.');
    fclose(fid);

end