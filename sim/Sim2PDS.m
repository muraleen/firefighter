clear all;
close all;
clc;

% Hardware Specifications
LiDARRange = [20 600]; % [cm] LiDAR Range
LiDARdtheta = 1; % [deg] LiDAR Angular Resolution
LiDARdr = 0.2; % [cm] LiDAR Linear Resolution

map = [];

% Simulation Parameters
run = 1; % run simulation flag
dt = 0.1; % [s] simulation time step
tth = 1; % [cm] Tolerance threshold
dth = 15; % [cm] First Pass discontinuity threshold
dth2 = 0.08; % Second Pass linearity threshold
wlt = 8; % [cm] Wall Length Tolerance
bounds = [0 244 0 244]; % [cm] Maze bounds (MATLAB axis)
RobotPos = [100 130]; % [cm] Position of Robot (x,y)
RobotHdg = -10; % [deg] Heading of Robot, with 0 deg pointing E, CCW +
LiDARMap = [transpose(0:359), zeros(360,1)+600];

% Define Arena Walls
walls = [    0   0   0 103; % W Boundary Lower Segment
             0 103   0 244; % W Boundary Upper Segment
             0 244  72 244; % N Boundary Left Segment
            72 244 244 244; % N Boundary Right Segment
           244   0 244 244; % E Boundary
             0   0 126   0; % S Boundary Left Segment
           126   0 244   0; % S Boundary Right Segment
%            0   0   0 244; % W Boundary
%            0   0 244   0; % S Boundary
%          244   0 244 244; % E Boundary
%            0 244 244 244; % N Boundary
         126   0 126  45; % Room 1 W Wall    [M]
         126  91 198  91; % Room 1 N Wall
           0 103  72 103; % Room 2 N Wall
          72  56  72 103; % Room 2 E Wall
          46 157  72 157; % Room 3 S Wall
          72 157  72 244; % Room 3 E Wall
         126 147 126 198; % Room 4 W Wall
         126 147 196 147; % Room 4 S Wall    [M]
         196 147 196 198; % Room 4 E Wall
         126 198 150 198]; % Room 4 N Wall   [M]

walls = [walls zeros(size(walls,1),1)];
for n = 1:size(walls,1)
    walls(n,5) = norm(walls(n,1:2) - walls(n,3:4));
end
     
while run
    
    clc;
    
    LiDARMap = [transpose(0:359), zeros(360,1)+600];
    
    % LiDAR Generate Range Map
    for theta = 0:359
        % Line12 - line from robot to LiDAR range along theta
        x1 = RobotPos(1);
        y1 = RobotPos(2);
        x2 = RobotPos(1) + LiDARRange(2)*cosd(RobotHdg + theta);
        y2 = RobotPos(2) + LiDARRange(2)*sind(RobotHdg + theta);
        
        for wall = 1:size(walls,1)            
            % Line34 - wall
            x3 = walls(wall,1);
            y3 = walls(wall,2);
            x4 = walls(wall,3);
            y4 = walls(wall,4);
            
            % Find Intersection point
            den = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
            numTerm1 = x1*y2 - y1*x2;
            numTerm2 = x3*y4 - y3*x4;
            Px = (numTerm1*(x3 - x4) - numTerm2*(x1 - x2))/den;
            Py = (numTerm1*(y3 - y4) - numTerm2*(y1 - y2))/den;
            
            % Check if intersection point is between bounds
            if ((Px >= x1-tth && Px <= x2+tth) || (Px >= x2-tth && Px <= x1+tth)) && ...
               ((Py >= y1-tth && Py <= y2+tth) || (Py >= y2-tth && Py <= y1+tth)) && ...
               ((Px >= x3-tth && Px <= x4+tth) || (Px >= x4-tth && Px <= x3+tth)) && ...
               ((Py >= y3-tth && Py <= y4+tth) || (Py >= y4-tth && Py <= y3+tth)) ;
                theta_corr = theta;
                if theta > 359
                    theta_corr = theta - 360;
                end
                d = round(sqrt((Px - RobotPos(1))^2 + (Py - RobotPos(2))^2)*5)/5;
                if d < LiDARMap(theta_corr+1,2)
%                     if d < LiDARRange(1)
%                         d = LiDARRange(1);
%                     end
                    LiDARMap(theta_corr+1,2) = d;
                end
            end
            
        end
    end
    
    for theta = 0:359
        d = LiDARMap(theta+1,2);
        if(d>LiDARRange(1) && d<LiDARRange(2))
            map = [map; RobotPos + d.*[cosd(RobotHdg+theta), sind(RobotHdg+theta)]];
        end
    end
    
    % LiDAR Map Filters and Processing
    % 2-Pass Discontinuity-based Segmentation Algorithm
    
    % FIRST PASS - Identify discontinuities    
    discLog_PASS1 = [];
    for theta = 1:359
        if (LiDARMap(theta,2) < LiDARMap(theta+1,2) - dth)
            % Outward Discontinuity
            discLog_PASS1 = [discLog_PASS1; [theta 1]];
        elseif (LiDARMap(theta,2) > LiDARMap(theta+1,2) + dth)
            % Inward Discontinuity
            discLog_PASS1 = [discLog_PASS1; [theta -1]];
        end
    end
    
    valid = zeros(360,1); % The first pass finds definite validity
    
    % Store definitely valid segments (between -1 and 1 on a CCW check)
    for n = 1:size(discLog_PASS1,1)
        if (n == size(discLog_PASS1,1))
            if (discLog_PASS1(n,2) == -1 && discLog_PASS1(1,2) == 1)
                % This segment is definitely valid (or atleast with good confidence)
                for theta = discLog_PASS1(n,1):359
                    valid(theta+1) = 1;
                end
                for theta = 0:(discLog_PASS1(1,1)-1)
                    valid(theta+1) = 1;
                end
            end
        else
            if (discLog_PASS1(n,2) == -1 && discLog_PASS1(n+1,2) == 1)
                % This segment is definitely valid (or atleast with good confidence)
                for theta = (discLog_PASS1(n,1)+1):discLog_PASS1(n+1,1)-1
                    valid(theta) = 1;
                end
            end
        end
    end
    
    % I'm not using the 2nd pass right now (the 2nd pass is to further
    % filter the data, but the 1st pass doesn't give incorrect lines - it
    % just also doesn't give all the correct lines. For now, it looks like
    % the data from the 1st pass is enough to identify walls.
    
%     % SECOND PASS - Check from first pass discontinuities until linearity
%     % ends or another discontinuity is found (just check linearity, then?)
%     invalid = zeros(360,1); % The second pass finds definite invalidity
%     for n = 1:size(discLog_PASS1,1)
%         if (discLog_PASS1(n,2) == -1 && discLog_PASS1(1,2) == 1)
%             for theta = (discLog_PASS1(n,1)+1):359
%                 % Linearity check
%                 % NOTE - p0: previous | p1: current
%                 p0 = LiDARMap(theta-1, 2).*[cosd(theta-1); sind(theta-1)];
%                 p1 = LiDARMap(theta, 2).*[cosd(theta); sind(theta)];
%                 % p0 and p1 are 2D vectors
%                 u = (p1-p0)./norm(p1-p0); % Unit vector in the direction of p1-p0
%                 % Calculate expected point by radial and projected line intersection
%                 % r*cos(theta) - ds*x_u = x_1 => | cos(theta) -x_u ||r | = |x_1|
%                 % r*sin(theta) - ds*y_u = y_1    | sin(theta) -y_u ||ds|   |y_1|
%                 %                                         A        * x   =   B
%                 %                                      => x = inv(A)*B = inv(A)*p1
%                 A = [[cosd(theta); sind(theta)] -u];
%                 x_vect = inv(A)*p1; % Unknown vector = [r_exp; ds]
%                 r_exp = x_vect(1); % [cm] Expected distance at next angle
%                 r_tol = r_exp*dth2; % [cm] R tolerance
%                 if (abs(LiDARMap(theta+1,2) - r_exp) > r_tol)
%                     break
%                 end
%                 invalid(theta) = 1;
%             end
%             for theta = 2:discLog_PASS1(1,1)
%                 % Linearity check
%                 % NOTE - p0: previous | p1: current
%                 p0 = LiDARMap(theta-1, 2).*[cosd(theta-1); sind(theta-1)];
%                 p1 = LiDARMap(theta, 2).*[cosd(theta); sind(theta)];
%                 % p0 and p1 are 2D vectors
%                 u = (p1-p0)./norm(p1-p0); % Unit vector in the direction of p1-p0
%                 % Calculate expected point by radial and projected line intersection
%                 % r*cos(theta) - ds*x_u = x_1 => | cos(theta) -x_u ||r | = |x_1|
%                 % r*sin(theta) - ds*y_u = y_1    | sin(theta) -y_u ||ds|   |y_1|
%                 %                                         A        * x   =   B
%                 %                                      => x = inv(A)*B = inv(A)*p1
%                 A = [[cosd(theta); sind(theta)] -u];
%                 x_vect = inv(A)*p1; % Unknown vector = [r_exp; ds]
%                 r_exp = x_vect(1); % [cm] Expected distance at next angle
%                 r_tol = r_exp*dth2; % [cm] R tolerance
%                 if (abs(LiDARMap(theta+1,2) - r_exp) > r_tol)
%                     break
%                 end
%                 invalid(theta) = 1;
%             end
%         elseif (n ~= size(discLog_PASS1,1) && discLog_PASS1(n,2) == 1) % Check CCW
%             for theta = (discLog_PASS1(n,1)+1):discLog_PASS1(n+1,1)
%                 % Linearity check
%                 % NOTE - p0: previous | p1: current
%                 p0 = LiDARMap(theta-1, 2).*[cosd(theta-1); sind(theta-1)];
%                 p1 = LiDARMap(theta, 2).*[cosd(theta); sind(theta)];
%                 % p0 and p1 are 2D vectors
%                 u = (p1-p0)./norm(p1-p0); % Unit vector in the direction of p1-p0
%                 % Calculate expected point by radial and projected line intersection
%                 % r*cos(theta) - ds*x_u = x_1 => | cos(theta) -x_u ||r | = |x_1|
%                 % r*sin(theta) - ds*y_u = y_1    | sin(theta) -y_u ||ds|   |y_1|
%                 %                                         A        * x   =   B
%                 %                                      => x = inv(A)*B = inv(A)*p1
%                 A = [[cosd(theta); sind(theta)] -u];
%                 x_vect = inv(A)*p1; % Unknown vector = [r_exp; ds]
%                 r_exp = x_vect(1); % [cm] Expected distance at next angle
%                 r_tol = r_exp*dth2; % [cm] R tolerance
%                 if (abs(LiDARMap(theta+1,2) - r_exp) > r_tol)
%                     break
%                 end
%                 invalid(theta) = 1;
%             end
%         elseif (n ~= 1 && discLog_PASS1(n,2) == -1) % Check CW
%             for theta = discLog_PASS1(n,1):-1:discLog_PASS1(n-1,1)
%                 % Linearity check
%                 % NOTE - p0: previous | p1: current
%                 p0 = LiDARMap(theta+1, 2).*[cosd(theta+1); sind(theta+1)];
%                 p1 = LiDARMap(theta, 2).*[cosd(theta); sind(theta)];
%                 % p0 and p1 are 2D vectors
%                 u = (p1-p0)./norm(p1-p0); % Unit vector in the direction of p1-p0
%                 % Calculate expected point by radial and projected line intersection
%                 % r*cos(theta) - ds*x_u = x_1 => | cos(theta) -x_u ||r | = |x_1|
%                 % r*sin(theta) - ds*y_u = y_1    | sin(theta) -y_u ||ds|   |y_1|
%                 %                                         A        * x   =   B
%                 %                                      => x = inv(A)*B = inv(A)*p1
%                 A = [[cosd(theta); sind(theta)] -u];
%                 x_vect = inv(A)*p1; % Unknown vector = [r_exp; ds]
%                 r_exp = x_vect(1); % [cm] Expected distance at next angle
%                 r_tol = r_exp*dth2; % [cm] R tolerance
%                 if (abs(LiDARMap(theta-1,2) - r_exp) > r_tol)
%                     break
%                 end
%                 invalid(theta) = 1;
%             end
%         end
%     end

    validLines = valid.*LiDARMap(:,2);
    
    % Incomplete lines have been filtered with the above 2PDS Algorithm,
    % now the valid lines need to be further separated into linear segments
    % (using a linearity check - as in the 2nd discontinuity pass above).
    
    segments = []; % [X1 Y1 X2 Y2 DS]
    start = 0;    
    
    % SEGMENT BY LINEARITY CHECK    
    for theta = 1:358
        if (validLines(theta) ~= 0) % VALID
            if start == 0
                start = theta;
            end
            % NOTE - p0: previous | p1: current
            p0 = LiDARMap(theta, 2).*[cosd(theta); sind(theta)];
            p1 = LiDARMap(theta+1, 2).*[cosd(theta+1); sind(theta+1)];
            % p0 and p1 are 2D vectors
            u = (p1-p0)./norm(p1-p0); % Unit vector in the direction of p1-p0
            % Calculate expected point by radial and projected line intersection
            % r*cos(theta) - ds*x_u = x_1 => | cos(theta) -x_u ||r | = |x_1|
            % r*sin(theta) - ds*y_u = y_1    | sin(theta) -y_u ||ds|   |y_1|
            %                                         A        * x   =   B
            %                                      => x = inv(A)*B = inv(A)*p1
            A = [[cosd(theta+1); sind(theta+1)] -u];
            x_vect = inv(A)*p1; % Unknown vector = [r_exp; ds]
            r_exp = x_vect(1); % [cm] Expected distance at next angle
            r_tol = r_exp*dth2; % [cm] R tolerance
            if (abs(LiDARMap(theta+2,2) - r_exp) > r_tol)
                % SEGMENT ENDED
                % [theta r_exp r_tol LiDARMap(theta+2,2) LiDARMap(theta+2,2)-r_exp (LiDARMap(theta+2,2)-r_exp)/r_tol]
                p0 = validLines(start).*[cosd(start) sind(start)];
                p1 = validLines(theta).*[cosd(theta) sind(theta)];
                segments = [segments; [p0 p1 norm(p1-p0)]];
                start = 0;
            end
        end
    end
    
    % All visible walls have been segmented - now they need to be compared
    % with the known map for identification
    
    % Compare lengths then inter-relation to other segments
    
    segments = [segments zeros(size(segments,1), size(walls,1))];
    
    % List walls for each segment ordered by similar lengths
    for n = 1:size(segments,1)
        simLenWalls = [];
        for w = 1:size(walls,1)
            % if (abs(walls(w,5) - segments(n,5)) < wlt)
                simLenWalls = [simLenWalls; [w abs(walls(w,5)-segments(n,5))]];
            % end
        end
        simLenWalls = sortrows(simLenWalls, 2);
        segments(n,6:(5+size(walls,1))) = simLenWalls(:,1)';
    end
    
    % Compare valid LiDAR segment distances to known wall distances
    for n = 1:size(segments,1)
        
    end
    
    % PLOTS
    figure(1);
    clf;
    subplot(2,2,1); % ARENA | SIMULATION VISUALIZATION
    hold on;
    
    % Draw Robot Position and Heading    
    plot(RobotPos(1), RobotPos(2), 'bo'); % Plot Rover
    plot([RobotPos(1) RobotPos(1)+(10.*cosd(RobotHdg))], [RobotPos(2) RobotPos(2)+(10.*sind(RobotHdg))], 'b-');
    
    % Draw Area Walls
    for wall = 1:size(walls,1)
        plot(walls(wall,[1 3]), walls(wall,[2 4]), 'Color', [0 0 0] ,'LineStyle', '-', 'LineWidth', 3);
    end
    
    hold off;
    grid on;
    axis(bounds);
    axis square;
    
    subplot(2,2,2); % LiDAR | VISIBLE RANGE MAP

    polar((LiDARMap(:,1) + 90).*(pi/180),LiDARMap(:,2),'r.');
    
    grid on;
    axis equal;
    
    subplot(2,2,3); % VALID LiDAR DATA AFTER 2-PASS DISCONTINUITY FILTER
    polar([90:449].*(pi/180), validLines', 'k.');
    
    subplot(2,2,4); % SEGMENTED LINES AFTER 2-PASS DISCONTINUITY FILTER
%     hold on;
%     for n = 1:size(segments,1)
%         plot(-[segments(n,2) segments(n,4)], [segments(n,1) segments(n,3)], 'k-');
%     end
%     hold off;
%     mD = max(LiDARMap(:,2));
%     axis([-mD mD -mD mD]);
%     axis square;
    
    % MAPPING PLOT - NOT REQ.
    plot(map(:,1), map(:,2), 'k.');
    grid on;
    axis(bounds);
    axis square;
    
    % AUTOMATIC CONTROL

    pause(dt);
    % RobotPos = RobotPos + [cosd(RobotHdg), sind(RobotHdg)];
    
    % MANUAL CONTROL
    
    ch = getkey;
    switch ch
        case 30 % UP
            RobotPos = RobotPos + 5.*[cosd(RobotHdg), sind(RobotHdg)];
        case 31 % DOWN
            RobotPos = RobotPos - 5.*[cosd(RobotHdg), sind(RobotHdg)];
        case 28 % LEFT
            RobotHdg = RobotHdg + 30;
        case 29 % RIGHT
            RobotHdg = RobotHdg - 30;
        otherwise % EXIT
            run = 0;
    end
    
    % run = 0;
    
    if ((RobotPos(1) < 0) || (RobotPos(1) > 244) || (RobotPos(2) < 0) || (RobotPos(2) > 244))
        run = 0;
    end

end