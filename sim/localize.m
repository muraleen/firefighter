file = 'scans/Scan6.txt'; % DATA FILE

map = [];

dd = 10; % [cm] Discontinuity check threshold
dr = 0.1; % Linearity check range threshold
da = 15; % [deg] Angularity tolerance
mR = 4; % Minimum repetition required to be valid
bounds = [0 244 0 244]; % [cm] Maze bounds (MATLAB axis)
LiDARMap = [transpose(0:359), zeros(360,1)];
res = 16; % [px] Map resolution

% Define Arena Walls
walls = [  0   0   0 244; % W Boundary
           0   0 244   0; % S Boundary
         244   0 244 244; % E Boundary
           0 244 244 244; % N Boundary
         126   0 126  45; % Room 1 W Wall    [M]
         126  91 198  91; % Room 1 N Wall
           0 103  72 103; % Room 2 N Wall
          72  56  72 103; % Room 2 E Wall
          46 157  72 157; % Room 3 S Wall
          72 157  72 244; % Room 3 E Wall
         126 147 126 198; % Room 4 W Wall
         126 147 196 147; % Room 4 S Wall    [M]
         196 147 196 198; % Room 4 E Wall
         126 198 150 198]; % Room 4 N Wall   [M]
         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% READ DATA FROM FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen(file);
data = transpose(fscanf(fid, '%d,%d\n', [2, inf]));
data(:,2) = data(:,2)/10;
fclose(fid);

for ii=1:size(data,1)
	LiDARMap(360-data(ii,1)+1,2) = data(ii,2);
end
     
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%% ARENA MAP PIXELIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%
pixMap_s = zeros(res); % Stored Pixel Map

ds = 244/res; 

for w = 1:size(walls,1)
    p1 = walls(w,1:2);
    p2 = walls(w,3:4);
    if p1(1) == p2(1) % V Line
        length = p2(2) - p1(2);
        for s = 0:round(length/ds);
            pixMap_s(1+round(p1(1)/ds),1+round(p1(2)/ds) + s) = 1;
        end
    else % H Line (There are only H and V Lines on this map)
        length = p2(1) - p1(1);
        for s = 0:round(length/ds);
            pixMap_s(1+round(p1(1)/ds) + s,1+round(p1(2)/ds)) = 1;
        end
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ONBOARD PROCESSING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Rotate LiDAR Map to H/V Lines

% Start by identifying the general complimentary angles to find the
% rotation angle

m = []; % Vector of Slopes

mapP = [];

for theta = 1:358
    % Make sure the linearity check is not at a discontinuity
    if (abs(LiDARMap(theta+2,2) - LiDARMap(theta+1,2)) < dd && abs(LiDARMap(theta+1,2) - LiDARMap(theta,2)) < dd)
        p0 = LiDARMap(theta,2).*[cosd(theta); sind(theta)];
        p1 = LiDARMap(theta+1, 2).*[cosd(theta+1); sind(theta+1)];
        u = (p1-p0)./norm(p1-p0);
        A = [[cosd(theta+1); sind(theta+1)] -u];
        x_vect = A\p1;
        r_exp = x_vect(1);
        r_tol = r_exp*dr;
        if abs(LiDARMap(theta+2,2) - r_exp) < r_tol
            % Grab angle and append to m vector
            m = [m; (180/pi)*atan2(u(2),u(1))];
            mapP = [mapP; p0' p1'];
        end
    end
end

% There should only be 4 angles in the data set but ofcourse,
% instrument and sensor errors can cause deviations (let's say +/- 10
% deg?) - so, I'm going to check if the next angle in within a certain
% range, it's going to average the value.

primAngles = [1, m(1)]; % [n AVG_ANGLE]

for n = 2:size(m,1)
    foundMatch = 0;
    for p = 1:size(primAngles,1)
        if (abs(primAngles(p,2)-m(n)) < da)
            primAngles(p,2) = (primAngles(p,1)*primAngles(p,2) + m(n))/(primAngles(p,1)+1);
            primAngles(p,1) = primAngles(p,1)+1; % Increment n
            foundMatch = 1;
            break
        end
    end
    if ~foundMatch
        primAngles = [primAngles; 1 m(n)];
    end
end

% Simple most popular average selection approximation for rot. angle
primAngles = sortrows(primAngles,-1);
rotAngle = primAngles(1,2);

% Now that the a complimentary (90 offset) rotation angle has been,
% generate a cartesian map of the LiDAR data offset by the rotation
% angle.
cartMap = zeros(360,2);
for theta = 1:360
    cartMap(theta,:) = LiDARMap(theta,2).*[cosd(theta-rotAngle) sind(theta-rotAngle)];
end

xRange = max(cartMap(:,2)) - min(cartMap(:,2));
yRange = max(cartMap(:,1)) - min(cartMap(:,1));

pixMap_l = zeros(floor(yRange/ds), floor(xRange/ds)); % LiDAR Data Pixel Map

for n = 1:size(cartMap,1)
    pixMap_l(1+round((cartMap(n,1) - min(cartMap(:,1)))/ds), 1+round((cartMap(n,2) - min(cartMap(:,2)))/ds)) = 1;
end

% Now that both the known and LiDAR maps have been pixelized to the
% same scale, they can be compared (at +0, +90, +180 and +270 deg) at
% every possible x and y offset to search for a match.

% For each of these angular and linear offsets, a similarity score can
% be calculated based on how many pixels overlap and the offset with
% the highest score can be selected to find the robot's position with
% relation to the map

rotMaps = {0, 0, 0, 0};

rotMaps{1} = pixMap_l;
for n = 2:4
    rotMaps{n} = rot90(rotMaps{n-1});
end

xL = size(pixMap_l,2); % LiDAR pixmap map x range
yL = size(pixMap_l,1); % LiDAR pixmap y range

xO = (res - xL) + 2; % Possible number of x-offset values
yO = (res - yL) + 2; % Possible number of y-offset values

rotScores = {0, 0, 0, 0};
rotMaxScores = zeros(1,4);

for r = 1:4 % For each angular/rotationl offset
    % Pre-allocate scores array for processing time optimization
    % Get rotated x and y offset values (flipped for certain rotations)
    if (r == 1 || r == 3)
        xR = xO;
        yR = yO;
        xRL = xL;
        yRL = yL;
    else
        xR = yO;
        yR = xO;
        xRL = yL;
        yRL = xL;
    end
    rotScores{r} = zeros(yR, xR);
    for x = 1:xR
        for y = 1:yR
            % Match Score calculation algorithm
            mapSeg = pixMap_s(y:(y+yRL-1),x:(x+xRL-1));
            score = sum(sum(mapSeg & rotMaps{r}));

            % Add score to rest of the score for comparison
            rotScores{r}(x,y) = score;
        end
    end
    rotMaxScores(r) = max(max(rotScores{r}));
end

% Get the rotational and linear offsets used which yield the highest
% match score.
rMax = find(rotMaxScores==max(rotMaxScores),1);
HDG_est = (rMax-1)*90 - rotAngle;

if HDG_est > 360
    HDG_est = HDG_est - 360;
elseif HDG_est < 0
    HDG_est = HDG_est + 360;
end

% Find the best score linear offset (x,y)
index = find(rotScores{rMax}==max(max(rotScores{rMax})),1);
xOffset = ceil(index/size(rotScores{rMax},1));
yOffset = index - (xOffset-1)*size(rotScores{rMax},1);

rMax

switch rMax
    case 1 % 0 OFF
        xPos_est = (xOffset-1)*ds - min(cartMap(:,1));
        yPos_est = (yOffset-1)*ds - min(cartMap(:,2));
    case 2 % 90 OFF
        xPos_est = (xOffset-1)*ds + xRange + min(cartMap(:,2));
        yPos_est = (yOffset-1)*ds - min(cartMap(:,1));
    case 3 % 180 OFF
        xPos_est = (yOffset-1)*ds + xRange + min(cartMap(:,1));
        yPos_est = (xOffset-1)*ds + yRange + min(cartMap(:,2));
    case 4 % 270 OFF
        xPos_est = (xOffset-1)*ds - min(cartMap(:,2));
        yPos_est = (yOffset-1)*ds + yRange + min(cartMap(:,1));
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PLOTS
figure(1);
clf;
subplot(2,2,1); % ARENA | SIMULATION VISUALIZATION
hold on;

plot(xPos_est, yPos_est, 'ro'); % Plot Rover's Estimated Position
plot([xPos_est xPos_est+(10.*cosd(HDG_est))], [yPos_est yPos_est+(10.*sind(HDG_est))], 'r-'); 

% Draw Area Walls
for wall = 1:size(walls,1)
    plot(walls(wall,[1 3]), walls(wall,[2 4]), 'Color', [0 0 0] ,'LineStyle', '-', 'LineWidth', 3);
end

hold off;
grid on;
axis(bounds);
axis square;

subplot(2,2,2); % LiDAR | VISIBLE RANGE MAP

polar((LiDARMap(:,1) + 90).*(pi/180),LiDARMap(:,2),'r.');

grid on;
axis equal;

%     % MAPPING PLOT - NOT REQ.
%     plot(map(:,1), map(:,2), 'k.');
%     grid on;
%     axis(bounds);
%     axis square;

subplot(2,2,3); % PIXELIZED MAP MATCHING VISUALIZATION
hold on;
for x = 0:res
    for y = 0:res
        if (pixMap_s(1+x,1+y) == 1)
            plot(x,y,'k*');
        end
    end
end
for x = 0:(size(rotMaps{rMax},1)-1)
    for y = 0:(size(rotMaps{rMax},2)-1)
        if (rotMaps{rMax}(1+x,1+y) == 1)
            plot(x+xOffset-1,y+yOffset-1,'r*');
        end
    end
end

hold off;
axis([-1 res+1 -1 res+1]);
axis square;
grid on;

subplot(2,2,4); % PIXELIZED MAP MATCHING VISUALIZATION
% polar((LiDARMap(:,1) + 90 - rotAngle).*(pi/180),LiDARMap(:,2),'r.');
hold on;
for x = 0:(size(pixMap_l,1)-1)
    for y = 0:(size(pixMap_l,2)-1)
        if (pixMap_l(1+x,1+y) == 1)
            plot(x,y,'k*');
        end
    end
end
hold off;

grid on;
axis([-1 res+1 -1 res+1]);
axis square;
