clear all;
close all;
clc;

% Hardware Specifications
LiDARRange = [20 600]; % [cm] LiDAR Range
LiDARdtheta = 1; % [deg] LiDAR Angular Resolution
LiDARdr = 0.2; % [cm] LiDAR Linear Resolution

map = [];

% Simulation Parameters
run = 1; % run simulation flag
dt = 0.5; % [s] simulation time step
TIME_ACCEL = 100;
tth = 1; % [cm] Thickness tolerance threshold
bounds = [0 244 0 244]; % [cm] Maze bounds (MATLAB axis)
RobotPos = [100 25]; % [cm] Position of Robot (x,y)
RobotHdg = 90; % [deg] Heading of Robot, with 0 deg pointing E, CCW +
LiDARMap = [transpose(0:359), zeros(360,1)+600];
turnDeg = 12;

tPass = 0;

CandlePos = [150 170];

D = 25;

TAPE_TOL = 4;

Kp_PROX = 6;
d_PROX = 25;

v = 0;
psi = 0;
a = 0.4;

irSensData = zeros(8,1);

% Define Arena Walls
walls = [  0   0   0 244; % W Boundary
           0   0 244   0; % S Boundary
         244   0 244 244; % E Boundary
           0 244 244 244; % N Boundary
         126   0 126  45; % SE Room W Wall    [M]
         126  91 198  91; % SE Room N Wall
           0 103  72 103; % SW Room N Wall
          72  56  72 103; % SW Room E Wall
          46 157  72 157; % NW Room S Wall
          72 157  72 244; % NW Room E Wall
         126 147 126 198; % NE Room W Wall
         126 147 196 147; % NE Room S Wall    [M]
         196 147 196 198; % NE Room E Wall
         126 198 150 198]; % NE Room N Wall   [M]

% Define white tapes (at doorways)
tapes = [ 72   0  72  56; % SW Room Entry
         126  45 126  91; % SE Room W Entry
         196  91 244  91; % SE Room N Entry
           0 157  46 156; % NW Room Entry
         147 198 196 198]; % NE Room Entry
     
figure(1);
% pause(10);
     
while run
    
    tPass = tPass + dt
    
    % clc;
    
    LiDARMap = [transpose(0:359), zeros(360,1)+600];
    
    % LiDAR Generate Range Map
    for theta = 0:359
        % Line12 - line from robot to LiDAR range along theta
        x1 = RobotPos(1);
        y1 = RobotPos(2);
        x2 = RobotPos(1) + LiDARRange(2)*cosd(RobotHdg + theta);
        y2 = RobotPos(2) + LiDARRange(2)*sind(RobotHdg + theta);
        
        for wall = 1:size(walls,1)            
            % Line34 - wall
            x3 = walls(wall,1);
            y3 = walls(wall,2);
            x4 = walls(wall,3);
            y4 = walls(wall,4);
            
            % Find Intersection point
            den = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
            numTerm1 = x1*y2 - y1*x2;
            numTerm2 = x3*y4 - y3*x4;
            Px = (numTerm1*(x3 - x4) - numTerm2*(x1 - x2))/den;
            Py = (numTerm1*(y3 - y4) - numTerm2*(y1 - y2))/den;
            
            % Check if intersection point is between bounds
            if ((Px >= x1-tth && Px <= x2+tth) || (Px >= x2-tth && Px <= x1+tth)) && ...
               ((Py >= y1-tth && Py <= y2+tth) || (Py >= y2-tth && Py <= y1+tth)) && ...
               ((Px >= x3-tth && Px <= x4+tth) || (Px >= x4-tth && Px <= x3+tth)) && ...
               ((Py >= y3-tth && Py <= y4+tth) || (Py >= y4-tth && Py <= y3+tth)) ;
                theta_corr = theta;
                if theta > 359
                    theta_corr = theta - 360;
                end
                d = round(sqrt((Px - RobotPos(1))^2 + (Py - RobotPos(2))^2)*5)/5;
                if d < LiDARMap(theta_corr+1,2)
%                     if d < LiDARRange(1)
%                         d = LiDARRange(1);
%                     end
                    LiDARMap(theta_corr+1,2) = d;
                end
            end
            
        end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%% NAVIGATION ALGORITHM %%%%%%%%%%%%%%%%%%%%%%%%%%

    mDist = 999;
    mDistTheta = -1;
    
    % Check if the robot is at a doorway (for white tape)
    tapeFlag = 0;
    lineSensPos = RobotPos + (D/2).*[cosd(RobotHdg) sind(RobotHdg)];
    lsx = lineSensPos(1);
    lsy = lineSensPos(2);
    for tape = 1:size(tapes,1)
        if lsx > tapes(tape,1)-TAPE_TOL && lsy > tapes(tape,2)-TAPE_TOL && lsx < tapes(tape,3)+TAPE_TOL && lsy < tapes(tape,4)+TAPE_TOL
            tapeFlag = 1;
        end
    end
    
    if tapeFlag
        tPass = 0;
        % STOP
        for v = 2.5:-a:0
            RobotPos = RobotPos + v.*dt.*[cosd(RobotHdg) sind(RobotHdg)];
            plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);
            pause(dt/TIME_ACCEL);
        end
        
        % CHECK FOR FIRE
        pause(0.5);
        % Check IR Sensor Values
        r2Candle = CandlePos - RobotPos;
        dist2Candle = norm(r2Candle);
        
        if dist2Candle < 100
            dir2Candle = atan2(r2Candle(2),r2Candle(1)) * (180/pi);
            if dir2Candle < 0
                dir2Candle = dir2Candle + 360;
            end

            for irSens = 1:8
                thSens = RobotHdg + (irSens*22.5);
                dTheta = dir2Candle - thSens;
                if dTheta > 180
                    dTheta = dTheta - 360;
                elseif dTheta < -180
                    dTheta = dTheta + 360;
                end
                if dTheta < 90 && dTheta > -90
                    irSensData(irSens) = (90-abs(dTheta))/(dir2Candle^2);
                else
                    irSensData(irSens) = 0;
                end
            end
            pause(0.5);
            if(dir2Candle > RobotHdg)
                for th = RobotHdg:turnDeg:dir2Candle
                    RobotHdg = th;
                    if RobotHdg > 360
                        RobotHdg = RobotHdg - 360;
                    elseif RobotHdg < 0
                        RobotHdg = RobotHdg + 360;
                    end
                    plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);
                    pause(dt/TIME_ACCEL);
                end
            else
                for th = RobotHdg:-turnDeg:dir2Candle
                    RobotHdg = th;
                    if RobotHdg > 360
                        RobotHdg = RobotHdg - 360;
                    elseif RobotHdg < 0
                        RobotHdg = RobotHdg + 360;
                    end
                    plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);
                    pause(dt/TIME_ACCEL);
                end
            end
            RobotHdg = dir2Candle;
            plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);
            disp('POOF!');
            break;
            run = 0;
        else
            irSensData = zeros(8,1);
            % NO FIRE
            pause(0.5);
            % Turn around 120 degrees and continue wall following
            for th = RobotHdg:turnDeg:RobotHdg+120
                RobotHdg = th;
                if RobotHdg > 360
                    RobotHdg = RobotHdg - 360
                elseif RobotHdg < 0
                    RobotHdg = RobotHdg + 360
                end
                plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);
                pause(dt/TIME_ACCEL);
            end
        end
        
    else
        vCmd = 2.5;
    
        % Analyze LiDAR Data (find direction to closest point)
        for n = 1:size(LiDARMap,1)
            theta = LiDARMap(n,1);
            if theta < 15 || theta > 165
                d = LiDARMap(n,2);
                if d < mDist
                    mDist = d;
                    mDistTheta = RobotHdg + theta;
                end
            end
        end

        psi = mDistTheta + 90 + Kp_PROX*(d_PROX-mDist);
    end
    
    if run
    
        if psi > 360
            psi = psi - 360;
        elseif psi < 0
            psi = psi + 360;
        end

        wCmd = psi - RobotHdg;
        if wCmd > 180
            wCmd = wCmd - 360;
        elseif wCmd < -180;
            wCmd = wCmd + 360;
        end

        % SIMPLE ROBOT DYNAMICS
        if wCmd > turnDeg
            RobotHdg = RobotHdg + turnDeg;
        elseif wCmd < -turnDeg
            RobotHdg = RobotHdg - turnDeg;
        else
            RobotHdg = psi;
        end

        if vCmd > v + a
            v = v + a;
        elseif vCmd < v - a
            v = v - a;
        else
            v = vCmd;
        end

        RobotPos = RobotPos + v.*dt.*[cosd(RobotHdg) sind(RobotHdg)];

        plotWorld(tapes, walls, RobotPos, RobotHdg, CandlePos, LiDARMap, D, bounds);

        % AUTOMATIC CONTROL

        pause(dt/TIME_ACCEL);    

        if ((RobotPos(1) < 0) || (RobotPos(1) > 244) || (RobotPos(2) < 0) || (RobotPos(2) > 244))
            run = 0;
        end
    
    end

end