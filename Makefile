# IMPORTANT NOTE - The RPLiDAR SDK must be compiled prior to running make here

# COMPILER
CC := g++

# RPLIDAR LIBRARY
RP_LIB := rplidar-sdk/output/Linux/Release/librplidar_sdk.a

# LIBRARIES
LIBS := -lstdc++ -lpthread -lrt -lserial

# MAIN FILE
SRC := src/main.cpp

# CUSTOM LIBRARY SOURCES
LIBSRC := src/matrix/matrix.cpp \
	  src/matrix/arrayops.cpp \
	  src/lidar/lidar.cpp
		  
# EXECUTABLE PATH
EXE := bin/firefighter

all:
	$(CC) $(SRC) $(LIBSRC) $(LIBS) $(RP_LIB) -o $(EXE)
	
clean:
	rm -rf bin/*o $(EXE) $(SIMEXE) $(LIDAREXE)

