#ifndef LIDAR_H
#define LIDAR_H

#include <stdio.h>
#include <stdlib.h>
#include "../../rplidar-sdk/sdk/include/rplidar.h"

using namespace rp::standalone::rplidar;

class LiDAR
{
	public:
		LiDAR(const char * port_path);						// Constructor
		~LiDAR();						// Destructor
		void scan();					// Scan Function
		float getdist(int n);			// Returns distance at theta = n
		
	private:
		float data[360];
		RPlidarDriver * drv;
		u_result scanResult;
		rplidar_response_measurement_node_t nodes[360*2];
		size_t count;
		void clearData();
};

#endif
