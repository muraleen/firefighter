#include <cmath>
#include "lidar.h"

#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

LiDAR::LiDAR(const char * port_path) {

	// Constructor
	for(int i=0; i<360; i++) {
		this->data[i] = 0;	// Set initial values to zero incase it's called
							// before a scan is complete
	}

	this->drv = RPlidarDriver::CreateDriver(RPlidarDriver::DRIVER_TYPE_SERIALPORT);
	
	_u32 baudrate = 115200;
	if IS_FAIL(this->drv->connect(port_path, baudrate)) {
		fprintf(stderr, "Error! LiDAR not connected to: %s.\n", port_path);
		exit(0);
	}
	
	this->count = _countof(nodes);
	
	this->drv->startScan(); // Start scanning!
}


LiDAR::~LiDAR() {
	RPlidarDriver::DisposeDriver(this->drv);
}


void LiDAR::scan() {
	this->scanResult = this->drv->grabScanData(this->nodes, this->count);
	
	if(IS_OK(this->scanResult) || this->scanResult == RESULT_OPERATION_TIMEOUT) {
		drv->ascendScanData(nodes, count);
		
		this->clearData();
		
		for (int i=0; i<(int)this->count; i++) {
			int theta = round((nodes[i].angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT)/64.0f);
			if (theta < 360) {
				this->data[theta] = this->nodes[i].distance_q2/4.0f;
			}
		}
		
		// printf("Completed Scanning...\n");
	} else {
		// printf("Scan Error: %x\n", this->scanResult);
	}
}

float LiDAR::getdist(int n) {
	return this->data[n];
}

void LiDAR::clearData() {
	for(int i=0; i<360; i++) {
		this->data[i] = 0;
	}
}
