#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <ctime>
#include <cstdlib>

#include <SerialStream.h>

#include "matrix/matrix.h"
#include "matrix/arrayops.h"

#include "lidar/lidar.h"

#define DEBUG 1

/* SERIAL COMMUNICATION PROTOCOL

>> [DATA BYTE] > CMD BYTE

DATA COMMANDS:     imply that the previous byte contains some kind of data, represented by the specific command byte

REQUIRED COMMANDS: request a certain variable from the receiving processor which needs to be sent back with the correct
                   data command byte
*/

// iMx6 --> SAM3X8 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define TXC_CLEAR 255
// > 254:  DATA: MIN FWD DISTANCE (between -15 and 15 degrees) FOR VELOCITY CTL
#define TXC_DATA_FWD_MDIST 254
// > 253:  DATA: MIN DIST POINT VALUE (CM)
#define TXC_DATA_VAL_MDIST 253
// > 252:  DATA: MIN DIST POINT DIRECTION (DEG)
#define TXC_DATA_DIR_MDIST 252
// > 251:  DATA: LOCALIZED POSITION (X)
#define TXC_DATA_LOC_X 251
// > 251:  DATA: LOCALIZED POSITION (Y)
#define TXC_DATA_LOC_Y 250
// > 251:  DATA: LOCALIZED POSITION (PSI)
#define TXC_DATA_LOC_PSI 249

/*HEADING: A heading transmission would be the sum of 2 consequetive bytes. Generally, the first byte is 200 if a second
          byte is required.
          
          For example, if the robot's heading is 313 degrees, the transmission would look like 200 113 5
*/

// VALID DATA TRANSMISSION BYTES HAVE DECIMAL VALUES BETWEEN 0 and 244

// SAM3X8 --> iMx6 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define RXC_CLEAR 255
// > 254:  REQUEST: LOCALIZED POSITION
#define RXC_REQUEST_LOC
// > 253:  REQUEST: AUTO-ALIGNING DPSI
#define RXC_REQUEST_AA_DPSI

#define BUFFER_SIZE 8

using namespace std;
using namespace LibSerial;

// DEFINE CONSTANTS
const static double R2D = 57.295779;
const static double PI = 3.14159;

char rxByte, txBuf[BUFFER_SIZE];
int rxCnt;

int n, theta, d;
int x, y, psi, psi1, psi2; // TRANSMISSION DATA BYTES
int lidarData[360][2] = {-1}; // LIDAR DATA ARRAY

int mDist, mDist_theta, mFwdDist; // mDIST ALGORITHM VARIABLES

int saturate(int val, int uLim, int lLim); // Define saturate function

int main(int argc, const char* argv[]) {

	system("clear");

	const char * lidar_port = NULL;
	
	// MANAGE COMMAND LINE ARGUMENTS
	if (argc > 1) {
		lidar_port = argv[1]; // Supplied by cmdline argument
	} else {
		lidar_port = "/dev/ttyUSB0"; // DEFAULT PORT
	}

    LiDAR lidar(lidar_port);
	
	SerialStream sam3x_UART("/dev/ttymxc3");
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not open serial port: /dev/ttymxc3" << endl;
	}
	
	sam3x_UART.SetBaudRate(SerialStreamBuf::BAUD_115200);
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not set baud rate for sam3x UART." << endl;
		exit;
	}

	// DEFAULT ARDUINO UART CONFIG: SERIAL_8N1
	sam3x_UART.SetCharSize(SerialStreamBuf::CHAR_SIZE_8);
	sam3x_UART.SetParity(SerialStreamBuf::PARITY_NONE);
	sam3x_UART.SetNumOfStopBits(1);
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not set SERIAL_8N1 cfg." << endl;
		exit;
	}

	// SET HARDWARE FLOW CONTROL
	sam3x_UART.SetFlowControl(SerialStreamBuf::FLOW_CONTROL_NONE);

	if(!sam3x_UART.good()) {
		cerr << "Error: Could not use hardware flow control." << endl;
		exit;
	}

	system("echo in > /sys/class/gpio/gpio123/direction");

	ifstream PRGM_INTERRUPT_GPIO("/sys/class/gpio/gpio123/value", ifstream::in);
	char PRGM_INTERRUPT_VAL;

    while(1) {

		// CHECK Program Interrupt Pin
		PRGM_INTERRUPT_VAL = PRGM_INTERRUPT_GPIO.get();
		PRGM_INTERRUPT_GPIO.seekg(0);
		// cout << "Reading Interrupt: " << PRGM_INTERRUPT_VAL_C << endl;
		// KEEP GPIO123 [D23] HIGH TO LET THE PROGRAM RUN AND SET IT LOW TO
		// INTERRUPT AND ENTER DEBIAN SHELL
		if(PRGM_INTERRUPT_VAL == '0') {
			return 0;
		}
    
    	rxCnt = 0;
    	// Get only [BUFFER_SIZE] bytes at a time so it leaves time to localize
    	while(sam3x_UART.rdbuf()->in_avail() > 0 && rxCnt < BUFFER_SIZE) {
			sam3x_UART.get(rxByte);
			// PROCESS INPUT
			rxCnt++;
		}
		
		// Fill LiDAR data array
		lidar.scan();
	
		n = 0;
		for(int th = 0; th < 360; th++) {
			d = lidar.getdist(th);
			if(d > 0) {
				lidarData[n][0] = th;
				lidarData[n][1] = d / 10;
				n++;
			}
		}
		
		// Find minimum distance point
		mDist = 999;
		mDist_theta = -1;
		n = 0;
		while(n < 360 && lidarData[n][1] > 0) {
			theta = lidarData[n][0];
			if(theta < 15 || theta > 165) {
				d = lidarData[n][1];
				if(d < mDist) {
					mDist = d;
					mDist_theta = theta;
				}
			}
		}
		
		// Find minimum forward distance (for optimal velocity calculation)
		mFwdDist = 999;
		n = 0;
		while(n < 360 && lidarData[n][1] > 0) {
			theta = lidarData[n][0];
			if(theta < 15 || theta > 345) { // +/- 15 degrees
				d = lidarData[n][1];
				if(d < mFwdDist) {
					mFwdDist = d;
				}
			}
		}
		
		// Saturate values to make them "safe" for transmission protocol
		mFwdDist = saturate(mFwdDist, 200, 0);
		mDist = saturate(mDist, 200, 0);
		mDist_theta = saturate(mDist_theta, 200, 0);
		
		// SEND PROCESSED LIDAR DATA TO SAM3X
		txBuf[0] = TXC_CLEAR;			// CLEAR BUFFER
		txBuf[1] = mFwdDist;			// mFwdDist DATA BYTE
		txBuf[2] = TXC_DATA_FWD_MDIST;	// CMD BYTE
		txBuf[3] = mDist;				// mDist DATA BYTE
		txBuf[4] = TXC_DATA_VAL_MDIST;	// CMD BYTE
		txBuf[5] = mDist_theta;			// mDist_theta DATA BYTE
		txBuf[6] = TXC_DATA_DIR_MDIST;	// CMD BYTE
		
		sam3x_UART.write(txBuf, 7);
		
		// DEBUG DATA
		if(DEBUG) {
			cout << "Min. Fwd. Distance: " << mFwdDist << endl;
			cout << "Closest Distance: " << mDist << endl;
			cout << "Closest Point Angle: " << mDist_theta << endl << endl;
		}
	
	}
		
	sam3x_UART.Close();	

    return 1;
}

int saturate(int val, int uLim, int lLim) {
	if(val > uLim) {
		return uLim;
	} else if(val < lLim) {
		return lLim;
	} else {
		return val;
	}
}
