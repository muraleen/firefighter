#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <ctime>
#include <cstdlib>

#include <SerialStream.h>

#include "matrix/matrix.h"
#include "matrix/arrayops.h"

#include "lidar/lidar.h"

/* SERIAL COMMUNICATION PROTOCOL

>> [DATA BYTE] > CMD BYTE

DATA COMMANDS:     imply that the previous byte contains some kind of data, represented by the specific command byte

REQUIRED COMMANDS: request a certain variable from the receiving processor which needs to be sent back with the correct
                   data command byte
*/

// iMx6 --> SAM3X8 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define TXC_CLEAR 255
// > 254:  DATA: MIN FWD DISTANCE (between -15 and 15 degrees) FOR VELOCITY CTL
#define TXC_DATA_FWD_MDIST 254
// > 253:  DATA: MIN DIST POINT VALUE (CM)
#define TXC_DATA_VAL_MDIST 253
// > 252:  DATA: MIN DIST POINT DIRECTION (DEG)
#define TXC_DATA_DIR_MDIST 252
// > 251:  DATA: LOCALIZED POSITION (X)
#define TXC_DATA_LOC_X 251
// > 251:  DATA: LOCALIZED POSITION (Y)
#define TXC_DATA_LOC_Y 250
// > 251:  DATA: LOCALIZED POSITION (PSI)
#define TXC_DATA_LOC_PSI 249

/*HEADING: A heading transmission would be the sum of 2 consequetive bytes. Generally, the first byte is 200 if a second
          byte is required.
          
          For example, if the robot's heading is 313 degrees, the transmission would look like 200 113 5
*/

// VALID DATA TRANSMISSION BYTES HAVE DECIMAL VALUES BETWEEN 0 and 244

// SAM3X8 --> iMx6 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define RXC_CLEAR 255
// > 254:  REQUEST: LOCALIZED POSITION
#define RXC_REQUEST_LOC
// > 253:  REQUEST: AUTO-ALIGNING DPSI
#define RXC_REQUEST_AA_DPSI

#define BUFFER_SIZE 8

using namespace std;
using namespace LibSerial;

// DEFINE CONSTANTS
const static double R2D = 57.295779;
const static double PI = 3.14159;

// DEFINE LOCALIZATION VARIABLES
int xPos, yPos, psi;
int n, d, map_x, map_y, mIndex, mList[400], pIndex, xRange, yRange, xMin, xMax, yMin, yMax, _x, _y;
float r_exp, r_tol, rotAngle;
matrix2 A;
vector2 p0, p1, u, x_vect, primAngles[400];
int xS, yS;
bool found;
int mScore = 0, mScore_xOff = 0, mScore_yOff = 0, mScore_rI = 0;

float ds = 244/((float) RES-1);

bool locSuccess = false;

char rxByte, txBuf[BUFFER_SIZE];
int rxCnt;

bool rotMap[4][RES][RES] = {0};
int lidarData[360][2] = {-1};
float lidarDataRotated[360][2] = {-1};
bool lidarPixelMap[RES][RES] = {0};
	
int psi1, psi2; // PSI DATA BYTES
	
#define TH_ANG_N 4
int TH_ANG_SEQ[TH_ANG_N] = {40, 1}; // , 30, 20};

bool pixelMap[RES][RES] =  {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
							{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,1,0,0,1,1,1,1,1,1,0,0,1},
							{1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1},
							{1,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1},
							{1,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1},
							{1,0,0,0,0,1,0,0,1,1,1,0,0,1,0,0,1},
							{1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
							{1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
							{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
	

/* The below variables should be used instead of the bools, this is a more robust system and can handle all map configurations
float rotMap[4][RES][RES] = {0};
int lidarData[360][2] = {-1};
float lidarDataRotated[360][2] = {-1};
float lidarPixelMap[RES][RES] = {0};
							
float pixelMap[RES][RES] = {{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.5,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.5,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.5,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.5,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,0.0,1.0},
							{1.0,1.0,1.0,1.0,1.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,1.0,1.0,1.0,0.0,0.0,1.0,0.5,0.5,1.0,1.0,1.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.0,0.5,0.5,1.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0},
							{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0}};
*/

// Declare functions
int localize(float TH_ANG);
int getMapMatchScore(int rotIndex, int xOff, int yOff, int _xS, int _yS);

int main(int argc, const char* argv[]) {

	system("clear");

	const char * lidar_port = NULL;
	
	// MANAGE COMMAND LINE ARGUMENTS
	if (argc > 1) {
		lidar_port = argv[1]; // Supplied by cmdline argument
	} else {
		lidar_port = "/dev/ttyUSB0"; // DEFAULT PORT
	}

    LiDAR lidar(lidar_port);
    
    // system("stty -F /dev/ttymxc3 cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts");
	
	SerialStream sam3x_UART("/dev/ttymxc3");
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not open serial port: /dev/ttymxc3" << endl;
	}
	
	sam3x_UART.SetBaudRate(SerialStreamBuf::BAUD_115200);
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not set baud rate for sam3x UART." << endl;
		exit;
	}

	// DEFAULT ARDUINO UART CONFIG: SERIAL_8N1
	sam3x_UART.SetCharSize(SerialStreamBuf::CHAR_SIZE_8);
	sam3x_UART.SetParity(SerialStreamBuf::PARITY_NONE);
	sam3x_UART.SetNumOfStopBits(1);
	if(!sam3x_UART.good()) {
		cerr << "Error: Could not set SERIAL_8N1 cfg." << endl;
		exit;
	}

	// SET HARDWARE FLOW CONTROL
	sam3x_UART.SetFlowControl(SerialStreamBuf::FLOW_CONTROL_NONE);

	if(!sam3x_UART.good()) {
		cerr << "Error: Could not use hardware flow control." << endl;
		exit;
	}

	system("echo in > /sys/class/gpio/gpio123/direction");

	ifstream PRGM_INTERRUPT_GPIO("/sys/class/gpio/gpio123/value", ifstream::in);
	char PRGM_INTERRUPT_VAL;

    while(1) {

	// CHECK Program Interrupt Pin
	PRGM_INTERRUPT_VAL = PRGM_INTERRUPT_GPIO.get();
	PRGM_INTERRUPT_GPIO.seekg(0);
	// cout << "Reading Interrupt: " << PRGM_INTERRUPT_VAL_C << endl;
	// KEEP GPIO123 [D23] HIGH TO LET THE PROGRAM RUN AND SET IT LOW TO
	// INTERRUPT AND ENTER DEBIAN SHELL
	if(PRGM_INTERRUPT_VAL == '0') {
		return 0;
	}
    
    	rxCnt = 0;
    	// Get only [BUFFER_SIZE] bytes at a time so it leaves time to localize
    	while(sam3x_UART.rdbuf()->in_avail() > 0 && rxCnt < BUFFER_SIZE) {
			sam3x_UART.get(rxByte);
			// PROCESS INPUT DATA
			// FIXME
			rxCnt++;
		}
		
		// Fill LiDAR data array
		lidar.scan();
		
		n=0;
		for(int theta=0; theta<360; theta++) {
			d = lidar.getdist(theta);
			if(d > 0) {
				lidarData[n][0] = theta;
				lidarData[n][1] = d/10;
				n++;
			}
		}
	
		// clock_t begin = clock();
		
		locSuccess = false;

		for(int i=0; i<TH_ANG_N; i++) {
			if(localize(TH_ANG_SEQ[i])) {
				locSuccess = true;
				break;
			}
		}

		if(locSuccess) {
			if(psi > 200) {
				psi1 = 200;
				psi2 = psi-200;
			} else {
				psi1 = psi;
				psi2 = 0;
			}
		
			// SEND LOCATION DATA TO SAM3X
			txBuf[0] = TXC_CLEAR;		// CLEAR BUFFER
			txBuf[1] = xPos;			// X DATA
			txBuf[2] = TXC_DATA_X;		// CMD BYTE
			txBuf[3] = yPos;			// Y DATA
			txBuf[4] = TXC_DATA_Y;		// CMD BYTE
			txBuf[5] = psi1;			// PSI DATA_1
			txBuf[6] = psi2;			// PSI DATA_2
			txBuf[7] = TXC_DATA_PSI;	// CMD BYTE
			
			sam3x_UART.write(txBuf, 8);
			
			cout << "Localization Successful! (" << xPos << ", " << yPos << ", " << psi << ")" << endl;
		} else {
			sam3x_UART.write(txBuf, 1);
			cout << "Insufficient score... discarding results!" << endl;
		}

		/* clock_t end = clock();
		float elapsed_sec = float(end-begin)/CLOCKS_PER_SEC;
		printf("\nElapsed Time: %1.6fs\n\n", elapsed_sec); */
		
		}
		
	sam3x_UART.Close();	

    return 1;
}

// Define functions
int localize(float TH_ANG) {
	// Run localization algorithm
	// STEP 1: Rotate LiDAR data to match H/V lines
	n = 0;
	mIndex = 0;
	while(n < 358 && lidarData[n+2][1] > 0) { // Distance data available
		if(mIndex > 360) break;
		if(abs(lidarData[n+2][1] - lidarData[n+1][1]) < TH_DSC && abs(lidarData[n+1][1] - lidarData[n][1] < TH_DSC)) {
			p0 = Matrix::v2create(lidarData[n][1]*cos(lidarData[n][0]/R2D), lidarData[n][1]*sin(lidarData[n][0]/R2D));
			p1 = Matrix::v2create(lidarData[n+1][1]*cos(lidarData[n+1][0]/R2D), lidarData[n+1][1]*sin(lidarData[n+1][0]/R2D));
			u = Matrix::v2dir(Matrix::v2subtract(p1, p0));
			A = Matrix::m2create((float) cos(lidarData[n+1][0]/R2D), -u.vars[0], (float) sin(lidarData[n+1][0]/R2D), -u.vars[1]);
			x_vect = Matrix::m2xv2(Matrix::m2inv(A), p1);
			r_exp = x_vect.vars[0];
			r_tol = r_exp*TH_LIN;
			if(abs(lidarData[n+2][1] - r_exp) < r_tol) {
				mList[mIndex] = R2D*Matrix::v2angle(u);
				mIndex++;
			}
		}
		n++;
	}
	
	// STEP 2: Find the primary slope angles from mList to select the final angle
	primAngles[0] = Matrix::v2create(1, mList[0]);
	pIndex = 0;
	for(int i=0; i<mIndex; i++) {
		found = false;
		for(int j=0; j<=pIndex; j++) {
			if(abs(primAngles[j].vars[1]-mList[i]) < TH_ANG) {
				primAngles[j].vars[1] = ((primAngles[j].vars[0]*primAngles[j].vars[1]) + mList[i])/(primAngles[j].vars[0]+1);
				primAngles[j].vars[0] = primAngles[j].vars[0] + 1;
				found = true;
				break;
			}
		}
		if(!found) primAngles[++pIndex] = Matrix::v2create(1, mList[i]);
	}
	
	rotAngle = primAngles[Matrix::v2maxIndex(primAngles, 360, 0)].vars[1];
	
	// STEP 3: Rotate the LiDAR map by the rotation angle calculated earlier
	n = 0;
	while(n < 360 && lidarData[n][1] > 0) {
		lidarDataRotated[n][0] = lidarData[n][1]*cos((lidarData[n][0]-rotAngle)/R2D);
		lidarDataRotated[n][1] = lidarData[n][1]*sin((lidarData[n][0]-rotAngle)/R2D);
		n++;
	}
	
	xMin = ArrayOps::getMinValue(lidarDataRotated, 360, 0);
	xMax = ArrayOps::getMaxValue(lidarDataRotated, 360, 0);
	yMin = ArrayOps::getMinValue(lidarDataRotated, 360, 1);
	yMax = ArrayOps::getMaxValue(lidarDataRotated, 360, 1);	
	xRange = yMax - yMin;
	yRange = xMax - xMin;
	
	for(int i=0; i<RES; i++) for(int j=0; j<RES; j++) lidarPixelMap[i][j] = 0;
	
	n = 0;
	while(n < 360 && !(lidarDataRotated[n][0] == 0 && lidarDataRotated[n][1] == 0)) {
		_x = round((lidarDataRotated[n][0] - xMin)/ds);
		_y = round((lidarDataRotated[n][1] - yMin)/ds);
		if(_x > RES-1) _x = RES-1;
		if(_y > RES-1) _y = RES-1;
		lidarPixelMap[_x][_y] = 1;
		n++;
	}
	
	xS = round(xRange/ds) + 1;
	yS = round(yRange/ds) + 1;	
	
	// STEP 4: Generate rotated versions of the map
	// MAP1: NO ROTATION : <yS,xS>
	for(int i=0; i<yS; i++) {
		for(int j=0; j<xS; j++) {
			rotMap[0][i][j] = lidarPixelMap[i][j];
		}
	}
	
	// ROTATE 90 degrees: <xS,yS>
	for(int i=0; i<yS; i++) {
		for(int j=0; j<xS; j++) {
			rotMap[1][j][yS-1-i] = lidarPixelMap[i][j];
		}
	}
	
	// ROTATE 180 degrees: <yS,xS>
	for(int i=0; i<xS; i++) {
		for(int j=0; j<yS; j++) {
			rotMap[2][j][xS-1-i] = rotMap[1][i][j];
		}
	}
	
	// ROTATE 90 degrees: <xS,yS>
	for(int i=0; i<yS; i++) {
		for(int j=0; j<xS; j++) {
			rotMap[3][j][yS-1-i] = rotMap[2][i][j];
		}
	}
	
	// STEP 5: Calculate map-match-scores for each rotation angle and pick the highest
	mScore = 0; // RESET SCORE TO 0
	for(int rI=0; rI<4; rI++) {
		int _xS = xS;
		int _yS = yS;
		if(rI%2 == 1) {
			_xS = yS;
			_yS = xS;
		}
		for(int xOff=0; xOff<=(RES-_xS); xOff++) {
			for(int yOff=0; yOff<=(RES-_yS); yOff++) {
				int score = getMapMatchScore(rI, xOff, yOff, _xS, _yS);
				if(score > mScore) {
					mScore = score;
					mScore_xOff = xOff;
					mScore_yOff = yOff;
					mScore_rI = rI;
				}
			}
		}
	}
	
	if(mScore > 15) {
		// Compute x, y and psi
		psi = (mScore_rI*90) + (90+rotAngle);
		if(psi > 306) psi -= 360;
		if(psi < 0) psi += 360;
		switch(mScore_rI) {
			case 0:
				xPos = (int) ((ds*mScore_xOff) - yMin);
				yPos = (int) ((ds*mScore_yOff) - xMin);
				break;
			case 1:
				xPos = (int) ((ds*mScore_xOff) + xMax);
				yPos = (int) ((ds*mScore_yOff) - yMin);
				break;
			case 2:
				xPos = (int) ((ds*mScore_xOff) + yMax);
				yPos = (int) ((ds*mScore_yOff) + xMax);
				break;
			case 3:
				xPos = (int) ((ds*mScore_xOff) - xMin);
				yPos = (int) ((ds*mScore_yOff) + yMax);
				break;
		}
		return 1;
	} else {
		return 0;
	}
}

int getMapMatchScore(int rotIndex, int xOff, int yOff, int _xS, int _yS) {
	int score = 0;
	for(int j=0; j<_yS; j++) {
		for(int i=0; i<_xS; i++) {
			if(pixelMap[yOff+j][xOff+i] & rotMap[rotIndex][j][i]) score += 1;
		}
	}
	return score;
}
