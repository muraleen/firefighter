#include "arrayops.h"

float ArrayOps::getMaxValue(float array[][2], int size, int col) {
	float max = -9999;
	for(int i=0; i<size; i++) {
		if(array[i][col] > max) max = array[i][col];
	}
	return max;
}

float ArrayOps::getMinValue(float array[][2], int size, int col) {
	float min = 9999;
	for(int i=0; i<size; i++) {
		if(array[i][col] < min) min = array[i][col];
	}
	return min;
}
