#include "matrix.h"
#include <cmath>

float Matrix::v2norm(vector2 a) {
	return  sqrt(pow(a.vars[0],2) + pow(a.vars[1],2));
}

vector2 Matrix::v2dir(vector2 a) {
	vector2 ans;
	ans.vars[0] = a.vars[0]/v2norm(a);
	ans.vars[1] = a.vars[1]/v2norm(a);
	return ans;
}

float Matrix::v2angle(vector2 a) {
	return atan2(a.vars[1], a.vars[0]);
}

vector2 Matrix::v2add(vector2 a, vector2 b) {
	vector2 ans;
	ans.vars[0] = a.vars[0] + b.vars[0];
	ans.vars[1] = a.vars[1] + b.vars[1];
	return ans;
}

vector2 Matrix::v2subtract(vector2 a, vector2 b) {
	vector2 ans;
	ans.vars[0] = a.vars[0] - b.vars[0];
	ans.vars[1] = a.vars[1] - b.vars[1];
	return ans;
}

matrix2 Matrix::m2create(float a, float b, float c, float d) {
	matrix2 ans;
	ans.vars[0][0] = a;
	ans.vars[0][1] = b;
	ans.vars[1][0] = c;
	ans.vars[1][1] = d;
	return ans;
}

vector2 Matrix::v2create(float a, float b) {
	vector2 ans;
	ans.vars[0] = a;
	ans.vars[1] = b;
	return ans;
}

matrix2 Matrix::m2zeros() {
	return m2create(0,0,0,0);
}

matrix2 Matrix::m2ones() {
	return m2create(1,1,1,1);
}

vector2 Matrix::v2zeros() {
	return v2create(0,0);
}

vector2 Matrix::v2ones() {
	return v2create(1,1);
}

matrix2 Matrix::m2identity() {
	return m2create(1,0,0,1);
}

int Matrix::v2maxIndex(vector2 list[], int size, int id) {
	float maxVal = -999;
	float maxIndex = -1;
	for(int i=0; i<size; i++) {
		if(list[i].vars[id] > maxVal) {
			maxVal = list[i].vars[id];
			maxIndex = i;
		}
	}
	return maxIndex;
} 

float Matrix::m2det(matrix2 A) {
	return (A.vars[0][0]*A.vars[1][1]) - (A.vars[0][1]*A.vars[1][0]);
}

matrix2 Matrix::m2inv(matrix2 A) {
	matrix2 ans;
	ans.vars[0][0] =  (1/m2det(A))*A.vars[1][1];
	ans.vars[0][1] = -(1/m2det(A))*A.vars[0][1];
	ans.vars[1][0] = -(1/m2det(A))*A.vars[1][0];
	ans.vars[1][1] =  (1/m2det(A))*A.vars[0][0];
	return ans;
}

matrix2 Matrix::m2trn(matrix2 A) {
	matrix2 ans;
	ans.vars[0][0] = A.vars[0][0];
	ans.vars[0][1] = A.vars[1][0];
	ans.vars[1][0] = A.vars[0][1];
	ans.vars[1][1] = A.vars[1][1];
	return ans;
}

matrix2 Matrix::m2xm2(matrix2 A, matrix2 B) {
	matrix2 ans;
	ans.vars[0][0] = (A.vars[0][0]*B.vars[0][0]) + (A.vars[0][1]*B.vars[1][0]);
	ans.vars[0][1] = (A.vars[0][0]*B.vars[0][1]) + (A.vars[0][1]*B.vars[1][1]);
	ans.vars[1][0] = (A.vars[1][0]*B.vars[0][0]) + (A.vars[1][1]*B.vars[1][0]);
	ans.vars[1][1] = (A.vars[1][0]*B.vars[0][1]) + (A.vars[1][1]*B.vars[1][1]);
	return ans;
}

vector2 Matrix::m2xv2(matrix2 A, vector2 b) {
	vector2 ans;
	ans.vars[0] = (A.vars[0][0]*b.vars[0]) + (A.vars[0][1]*b.vars[1]);
	ans.vars[1] = (A.vars[1][0]*b.vars[0]) + (A.vars[1][1]*b.vars[1]);
	return ans;
}
