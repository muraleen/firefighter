#ifndef ARRAYOPS_H
#define ARRAYOPS_H

class ArrayOps {
	public:
		static float getMaxValue(float array[][2], int size, int col);
		static float getMinValue(float array[][2], int size, int col);
};

#endif // ARRAYOPS_H
