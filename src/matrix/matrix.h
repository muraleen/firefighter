#ifndef MATRIX_H
#define MATRIX_H

struct vector2 {
	float vars[2];
};

struct matrix2 {
	float vars[2][2];
};

class Matrix {
	public:
		static float v2norm(vector2 a);
		static vector2 v2dir(vector2 a);
		static float v2angle(vector2 a);
		static matrix2 m2create(float a, float b, float c, float d);
		static vector2 v2create(float a, float b);
		static vector2 v2add(vector2 a, vector2 b);
		static vector2 v2subtract(vector2 a, vector2 b);
		static matrix2 m2zeros();
		static matrix2 m2ones();
		static vector2 v2zeros();
		static vector2 v2ones();
		static matrix2 m2identity();
		static int v2maxIndex(vector2 list[], int size, int id);
		static float m2det(matrix2 A);
		static matrix2 m2inv(matrix2 A);
		static matrix2 m2trn(matrix2 A);
		static matrix2 m2xm2(matrix2 A, matrix2 B);
		static vector2 m2xv2(matrix2 A, vector2 b);
};

#endif // MATRIX_H
