#include <Scheduler.h>
#include <Robot.h>

// By default, motor driver communication is made on UART[3]

#define WHEEL_R  0.045  // [m] wheel radius
#define WHEEL_B  0.130  // [m] wheel base radius
// Encoder signal pins
#define ENC_L_A  2
#define ENC_L_B  3
#define ENC_R_A  4
#define ENC_R_B  5
#define LINE_SENSOR  A8  // Line Sensor pin

#define CRZ_SPEED 0.40 // [m/s] Default "cruise" speed
#define TURN_RATE 40 // [DUTY CYCLE] Maximum turn rate

#define RX_BUF_SIZE 2

// Define room entry identification nodes
#define NODE_NW_MAIN 0
#define NODE_SE_N    4
#define NODE_SE_W    5
#define NODE_SW_MAIN 8
#define NODE_NE_MAIN 12

/* SERIAL COMMUNICATION PROTOCOL

>> [DATA BYTE] > CMD BYTE

DATA COMMANDS:     imply that the previous byte contains some kind of data, represented by the specific command byte

REQUIRED COMMANDS: request a certain variable from the receiving processor which needs to be sent back with the correct
                   data command byte
*/

// iMx6 --> SAM3X8 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define RXC_CLEAR 255
// > 254:  BOOT PROCESS COMPLETE | STAND BY
#define RXC_STBY 254
// > 253:  START SEQUENCE SIGNAL RECEIVED (Either by audio or manual override button)
#define RXC_START 253
// > 252:  STANDARD STOP COMMAND (Immediate stop command is sent through an external interrupt pin)
#define RXC_STOP 252
// > 251:  DATA: X-POSITION (CM)
#define RXC_DATA_X 251
// > 250:  DATA: Y-POSITION (CM)
#define RXC_DATA_Y 250
// > 249:  DATA: HEADING [DEG]*
#define RXC_DATA_PSI 249
// > 248:  DATA: MAP INDEX (Means the map was successfully recognized)
#define RXC_DATA_MAP 248
// > 247:  REQUEST: dX SINCE LAST REQUEST
#define RXC_REQUEST_DX 247
// > 246:  REQUEST: dY SINCE LAST REQUEST
#define RXC_REQUEST_DY 246
// > 245:  REQUEST: dpsi SINCE LAST REQUEST
#define RXC_REQUEST_DPSI 245
// > 244:  INITIAL POSITION ACQUIRED: Cleared to start navigation algorithm
#define RXC_INIT_POS_ACQ 244
// > 243:  LOCALIZATION FAILED: Move a little for a re-try
#define RXC_LOC_FAILED 243

// VALID DATA TRANSMISSION BYTES HAVE DECIMAL VALUES BETWEEN 0 and 200

// SAM3X8 --> iMx6 COMMAND BYTE MAPPING:

// > 255:  CLEAR BUFFER
#define TXC_CLEAR 255
// > 254:  START/RESUME LOCALIZATION ALGORITHM
#define TXC_START_LOC 254
// > 253:  PAUSE LOCALIZATION ALGORITHM (triggered when a room is reached)
#define TXC_PAUSE_LOC 253
// > 252:  DATA: dX SINCE LAST REQUEST [RESPONSE TO 7]
#define TXC_DATA_DX 252
// > 251:  DATA: dy SINCE LAST REQUEST [RESPONSE TO 8]
#define TXC_DATA_DY 251
// > 250:  DATA: dpsi SINCE LAST REQUEST [RESPONSE TO 9]
#define TXC_DATA_DPSI 250
// > 249:  DEBUG MSG: PATH PLANNING STARTED
#define TXC_PATH_PLANNING_STARTED 249
// > 248:  DATA: WPT REACHED (moving to next)
#define TXC_DATA_WPT_REACHED 248
// > 247:  DEBUG MSG: TARGET REACHED
#define TXC_TARGET_REACHED 247
// > 246:  DEBUG_MSG: CHAR STRING MESSAGE
#define TXC_DEBUG_MSG 246
// > 202:  DEBUG MSG: PATH PLANNING COMPLETE, WAIT FOR PATH
#define TXC_PATH_PLANNING_COMPLETE 202 // Then, sends the nodes on the path
// > 201:  DATA: SELECTED PATH
#define TXC_DATA_PATH 201 // The data sent between 99 and 100 are nodes on the path

/*
*HEADING: A heading transmission would be the sum of 2 consequetive bytes. Generally, the first byte is 200 if a second
          byte is required.
          
          For example, if the robot's heading is 313 degrees, the transmission would look like 200 113 5
*/

byte rxByte, rxBuffer[RX_BUF_SIZE] = {-1};

bool overTape;
bool _run = false;

int dx, dy, dpsi, _data;
int mapIndex = 0;
/* POSSIBLE MAP INDICES
>  0: No Idea so far
>  1: MAP: 1
>  2: MAP: 2
>  3: MAP: 3
>  4: MAP: 4
>  5: POSSIBLE MAPS: 1 -OR- 2
>  6: POSSIBLE MAPS: 1 -OR- 3
>  7: POSSIBLE MAPS: 1 -OR- 4
>  8: POSSIBLE MAPS: 2 -OR- 3
>  9: POSSIBLE MAPS: 2 -OR- 4
> 10: POSSIBLE MAPS: 3 -OR- 4
> 11: POSSIBLE MAPS: 1, 2 -OR- 3
> 12: POSSIBLE MAPS: 1, 2 -OR- 4
> 13: POSSIBLE MAPS: 1, 3 -OR- 4
> 14: POSSIBLE MAPS: 2, 3 -OR- 4 */

#define NUM_NODES 14 // GENERIC NODS + HOME NODE

// NAVIGATIONAL NODES
int navNodes[NUM_NODES][2] =  {{ 20, 155}, // NODE 0: NW Room - main entry
                               { 20, 124}, // NODE 1
                               {100, 124}, // NODE 2
                               {220, 124}, // NODE 3
                               {220,  90}, // NODE 4: SE Room - N entry
                               {125,  70}, // NODE 5: SE Room - W entry
                               {100,  70}, // NODE 6
                               {100,  20}, // NODE 7
                               { 70,  20}, // NODE 8: SW Room - main entry
                               {100, 225}, // NODE 9
                               {172, 225}, // NODE 10
                               {220, 225}, // NODE 11
                               {172, 196}, // NODE 12: NE Room - main entry
                               {  0,   0}};// NODE 13: HOME (ACCESS WITH navNodes[NUM_NODES-1])

#define NUM_PATHS 15 // GENERIC PATHS + HOME NODE PATH

// NAVIGATIONAL PATHS (connect 2 nodes) {NODE1, NODE2, PATH_LENGTH}
int navPaths[NUM_PATHS][2] =  {{ 0,  1},
                               { 1,  2},
                               { 2,  3},
                               { 2,  6},
                               { 2,  9},
                               { 3,  4},
                               { 3, 11},
                               { 5,  6},
                               { 6,  7},
                               { 7,  8},
                               { 9, 10},
                               {10, 11},
                               {10, 12},
                               { 0, NUM_NODES-1}}; // HOME PATH (connects home to nearest node: ACCESS WITH navPaths[NUM_PATHS-1])

int possiblePaths[8][12] = {0}; // Upto 8 possible paths, with a maximum of 10 nodes each (ofcourse, there's a
                                // little extra to prevent memory overflow and malloc errors, the first column
                                // of each possible path defines the length of the path and the second is just
                                // an impossible value (-1) for the purpose of the path-planning algorithm, the
                                // actual first node starts on col 2 (the third column)
int nPaths = 0;

// Navigation Algorithm Variables
int _lastWpt = -1;
bool _pathReady = false;
bool _nextNodeFound = false;
bool _moveToTarget = false;
int currentWpt = 0;
int numWpts = 0;
int finalPath[10] = {-1};
int targetNode = -1;
float tgt_x = 0, tgt_y = 0;
float ftgt_x = 0, ftgt_y = 0;
float _x=0, _y=0, _psi=0;

Robot eagle1(WHEEL_R, WHEEL_B, ENC_L_A, ENC_L_B, ENC_R_A, ENC_R_B, TURN_RATE);

void setup() {
  Serial.begin(115200);
  delay(1000);
  pinMode(52, OUTPUT);
  // INIT LED SEQUENCE
  for(int i=0; i<5; i++) {
    digitalWrite(52, LOW);
    delay(50);
    digitalWrite(52, HIGH);
    delay(50);
  }
  eagle1.init();
  eagle1.stopMotors();
  eagle1.vMode = false;
  digitalWrite(52, LOW);
  eagle1.x = 0.20;
  eagle1.y = 0.10;
  eagle1.psi = PI/2;
  
  setThisAsHome();
  
  goToNode(0);
  
  // Start scheduler threads
  Scheduler.startLoop(robotKinematics);
  Scheduler.startLoop(navigate);
}

void loop() {
  // SERIAL COMM THREAD [MAIN]
  if(Serial.available()) {
    rxByte = Serial.read();
    switch(rxByte) {
      case RXC_CLEAR:
        clearBuffer();
        break;
      case RXC_STBY:
        _run = false;
        break;
      case RXC_START:
        _run = true;
        break;
      case RXC_STOP:
        eagle1.vMode = false;
        eagle1.accelToSpeed(2,2,0,0,20);
        _run = false;
        break;
      case RXC_DATA_X:
        eagle1.x = (int) rxBuffer[0];
        clearBuffer();
        break;
      case RXC_DATA_Y:
        eagle1.x = (int) rxBuffer[0];
        clearBuffer();
        break;
      case RXC_DATA_PSI:
        _data = ((int) rxBuffer[0]) + ((int) rxBuffer[1]);
        eagle1.psi = ((float) rxBuffer[0])*(PI/180);
        clearBuffer();
        break;
      case RXC_DATA_MAP:
        mapIndex = (int) rxBuffer[0];
        clearBuffer();
        break;
      case RXC_REQUEST_DX:
        dx = (int) ((eagle1.x - _x)*100);
        _x = eagle1.x;
        Serial.write((byte) TXC_CLEAR);
        Serial.write((byte) dx);
        Serial.write((byte) TXC_DATA_DX);
        break;
      case RXC_REQUEST_DY:
        dy = (int) ((eagle1.y - _y)*100);
        _y = eagle1.y;
        Serial.write((byte) TXC_CLEAR);
        Serial.write((byte) dy);
        Serial.write((byte) TXC_DATA_DY);
        break;
      case RXC_REQUEST_DPSI:
        dpsi = (int) ((eagle1.psi - _psi)*(180/PI));
        _psi = eagle1.psi;
        Serial.write((byte) TXC_CLEAR);
        Serial.write((byte) dpsi);
        Serial.write((byte) TXC_DATA_DPSI);
        break;
      case RXC_INIT_POS_ACQ:
        // NOTHING TO DO AT THE MOMENT
        break;
      case RXC_LOC_FAILED:
        // NOTHING TO DO AT THE MOMENT
        break;
      default:
        for(int bufIndex = 0; bufIndex < RX_BUF_SIZE; bufIndex++) {
          if(rxBuffer[bufIndex] < 0) {
            rxBuffer[bufIndex] = rxByte;
            break;
          }
        }
        // HANDLE BUFFER OVERFLOW [LATER]
    }
  }
  
  /* DEBUG POSITION:
  Serial.print("x: "); Serial.print(eagle1.x);
  Serial.print("\ty: "); Serial.print(eagle1.y);
  Serial.print("\tpsi: "); Serial.println(eagle1.psi*(180/PI)); */
  
  delay(1); // REQUIRED FOR SCHEDULER << DO NOT REMOVE!
}

void clearBuffer() {
  for(int bufIndex = 0; bufIndex < RX_BUF_SIZE; bufIndex++) rxBuffer[bufIndex] = -1;
}

// ROBOT NAVIGATION ALGORITHM
void navigate() {
  if(_moveToTarget) {
    // NAVIGATION PATH-PLANNING ALGORITHM
    if(!_pathReady) {
      for(int i=0; i<10; i++) {
        finalPath[i] = -1;
      }
      numWpts = 0;
      // Find the optimal path to the target node
      // IF the robot is in a room, set the first node to the room's entry point
      if(inNWRoom()) {
        possiblePaths[0][2] = NODE_NW_MAIN;
      } else if(inSWRoom()) {
        possiblePaths[0][2] = NODE_SW_MAIN;
      } else if(inSERoom()) {
        float d2NEntry = sqrt(pow(navNodes[NODE_SE_N][0] - (eagle1.x*100),2) + pow(navNodes[NODE_SE_N][1] - (eagle1.y*100),2));
        float d2WEntry = sqrt(pow(navNodes[NODE_SE_W][0] - (eagle1.x*100),2) + pow(navNodes[NODE_SE_W][1] - (eagle1.y*100),2));
        if(d2NEntry < d2WEntry) {
          possiblePaths[0][2] = NODE_SE_N;
        } else {
          possiblePaths[0][2] = NODE_SE_W;
        }
      } else if(inNERoom()) {
        possiblePaths[0][2] = NODE_NE_MAIN;
      } else {
      // ELSE, set the first node to the closest node to the robot
        float dMin = 999; // Start with some number that's always above the min distance
        int node1 = -1;
        float d2Node;
        for(int node=0; node<NUM_NODES; node++) {
          if((node!=NODE_NW_MAIN) && (node!=NODE_SW_MAIN) && (node!=NODE_SE_W) && (node!=NODE_SE_N) && (node!=NODE_NE_MAIN)) { // Ignore room entry nodes
            d2Node = sqrt(pow(navNodes[node][0] - (eagle1.x*100),2) + pow(navNodes[node][1] - (eagle1.y*100),2));
            if(d2Node < dMin) {
              node1 = node;
            }
          }
        }
        possiblePaths[0][2] = node1;
      }
      if(possiblePaths[0][2] == targetNode) { // Check if the closest node IS the target node
        finalPath[0] = targetNode;
        numWpts = 1;
      } else {
        possiblePaths[0][0] = 1; // Set path 0 length to 1
        possiblePaths[0][1] = -1; // Set path 0 length to 1
        nPaths = 1; // Set number of paths to 1
        // Now, the actual path planning algorithm...
        while(!_pathReady) { // This is an iterative process
          for(int pathIndex=0; pathIndex<nPaths; pathIndex++) {
            _nextNodeFound = false;
            for(int path=0; path<NUM_PATHS; path++) {
              if((navPaths[path][0] == possiblePaths[pathIndex][possiblePaths[pathIndex][0]+1]) && (navPaths[path][1] != possiblePaths[pathIndex][possiblePaths[pathIndex][0]])) {
                if(navPaths[path][1] == targetNode) {
                  // Reached target, copy to final path and set number of waypoints
                  int n;
                  for(n=0; n<possiblePaths[pathIndex][0]; n++) {
                    finalPath[n] = possiblePaths[pathIndex][n+2];
                  }
                  finalPath[n] = targetNode;
                  numWpts = n+1;
                  _pathReady = true;
                  break;
                } else {
                  if(possiblePaths[pathIndex][0] < 10) {
                    if(!_nextNodeFound) {
                      // Add next node to the current possible path and include possible path length
                      possiblePaths[pathIndex][possiblePaths[pathIndex][0]+2] = navPaths[path][1];
                      possiblePaths[pathIndex][0]++;
                      _nextNodeFound = true;
                    } else {
                      // Fork current path and add the next node to the new path
                      int n;
                      for(n=0; n<possiblePaths[pathIndex][0]+2; n++) {
                        possiblePaths[nPaths][n] = possiblePaths[pathIndex][n];
                      }
                      possiblePaths[nPaths][n] = navPaths[path][1];
                      possiblePaths[nPaths][0]++;
                      nPaths++;
                    }
                  }
                }
              } else if((navPaths[path][1] == possiblePaths[pathIndex][possiblePaths[pathIndex][0]+1]) && (navPaths[path][0] != possiblePaths[pathIndex][possiblePaths[pathIndex][0]])) {
                if(navPaths[path][0] == targetNode) {
                  // Reached target, copy to final path and set number of waypoints
                  int n;
                  for(n=0; n<possiblePaths[pathIndex][0]; n++) {
                    finalPath[n] = possiblePaths[pathIndex][n+2];
                  }
                  finalPath[n] = targetNode;
                  numWpts = n+1;
                  _pathReady = true;
                  break;
                } else {
                  if(possiblePaths[pathIndex][0] < 10) {
                    if(!_nextNodeFound) {
                      // Add next node to the current possible path and include possible path length
                      possiblePaths[pathIndex][possiblePaths[pathIndex][0]+2] = navPaths[path][0];
                      possiblePaths[pathIndex][0]++;
                      _nextNodeFound = true;
                    } else {
                      // Fork current path and add the next node to the new path
                      int n;
                      for(n=0; n<possiblePaths[pathIndex][0]+2; n++) {
                        possiblePaths[nPaths][n] = possiblePaths[pathIndex][n];
                      }
                      possiblePaths[nPaths][n] = navPaths[path][0];
                      possiblePaths[nPaths][0]++;
                      nPaths++;
                    }
                  }
                }
              }
            }
          }
        }
      }
      
      /* TEST PATH [COMMENT OUT WHEN NOT BEING USING]
      finalPath[0] = 8;
      finalPath[1] = 7;
      finalPath[2] = 6;
      finalPath[3] = 2;
      finalPath[4] = 1;
      finalPath[5] = 0;
      numWpts = 6; */
      
      // Copy target node data to specific variables
      ftgt_x = ((float) navNodes[targetNode][0])/100.0;
      ftgt_y = ((float) navNodes[targetNode][1])/100.0;
      tgt_x = ((float) navNodes[finalPath[0]][0])/100.0;
      tgt_y = ((float) navNodes[finalPath[0]][1])/100.0;

      currentWpt = 0;
      _pathReady = true;
      
    }
  
    // MOVE-TO-TARGET ALGORITHM
    eagle1.vMode = true;
    float d2ftgt = sqrt(pow(ftgt_x - eagle1.x,2) + pow(ftgt_y - eagle1.y,2));
    if(d2ftgt < 0.08) {
      eagle1.setTargetVelocity(0);
    } else if(d2ftgt < CRZ_SPEED) {
      eagle1.setTargetVelocity(d2ftgt);
    } else {
      eagle1.setTargetVelocity(CRZ_SPEED);
    }
    // Turn to correct heading
    float headingToTarget = atan2(tgt_y - eagle1.y, tgt_x - eagle1.x);
    if(headingToTarget < 0) {
      headingToTarget += 2*PI;
    }
    eagle1.setTargetHeading(headingToTarget);
    
    // WAYPOINT/NODE TRANSITION
    // FIXME
    float d2tgt = sqrt(pow(tgt_x - eagle1.x,2) + pow(tgt_y - eagle1.y,2));
    if(currentWpt < numWpts-1) {
      if(d2tgt < 0.15) {
        digitalWrite(52, HIGH);
        currentWpt++;
        tgt_x = ((float) navNodes[finalPath[currentWpt]][0])/100.0;
        tgt_y = ((float) navNodes[finalPath[currentWpt]][1])/100.0;
        delay(10);
        digitalWrite(52, LOW);
      }
    } else if(currentWpt == numWpts-1) {
      if(d2tgt < 0.05) {
        digitalWrite(52, HIGH);
        currentWpt++;
        delay(10);
        digitalWrite(52, LOW);
      }
    } else {
      _moveToTarget = false;
      eagle1.vMode = false;
      eagle1.stopMotors();
      digitalWrite(52, HIGH);
      delay(1000);
      digitalWrite(52, LOW);
    } 
  }
  yield();
}

// ROBOT KINEMATICS AND ODOMETRY CALCULATION THREAD
void robotKinematics() {
  eagle1.run();
  yield();
}

void goToNode(int node) {
  targetNode = node;
  _pathReady = false;
  _moveToTarget = true;
}

void returnHome() { // SET RETURN HOME VARIABLES
  goToNode(NUM_NODES-1);
}

void setThisAsHome() { // MUST BE CALLED ON SETUP()!!!
  // SET HOME NODE LOCATION
  navNodes[NUM_NODES-1][0] = (int) (eagle1.x*100.0);
  navNodes[NUM_NODES-1][1] = (int) (eagle1.y*100.0);
  Serial.print("SET HOME: X: ");
  Serial.print(navNodes[NUM_NODES-1][0]);
  Serial.print("\tY: ");
  Serial.println(navNodes[NUM_NODES-1][1]);
  
  int nodeNearHome=0;
  
  // FIND NODE NEAREST TO HOME
  if(inNWRoom()) {
    nodeNearHome = NODE_NW_MAIN;
  } else if(inSWRoom()) {
    nodeNearHome = NODE_SW_MAIN;
  } else if(inSERoom()) {
    float d2NEntry = sqrt(pow(navNodes[NODE_SE_N][0] - (eagle1.x*100),2) + pow(navNodes[NODE_SE_N][1] - (eagle1.y*100),2));
    float d2WEntry = sqrt(pow(navNodes[NODE_SE_W][0] - (eagle1.x*100),2) + pow(navNodes[NODE_SE_W][1] - (eagle1.y*100),2));
    if(d2NEntry < d2WEntry) {
      nodeNearHome = NODE_SE_N;
    } else {
      nodeNearHome = NODE_SE_W;
    }
  } else if(inNERoom()) {
    nodeNearHome = NODE_NE_MAIN;
  } else {
  // ELSE, set the first node to the closest node to the robot
    float dMin = 999; // Start with some number that's always above the min distance
    float d2Node;
    for(int node=0; node<NUM_NODES-1; node++) {
      if((node!=NODE_NW_MAIN) && (node!=NODE_SW_MAIN) && (node!=NODE_SE_W) && (node!=NODE_SE_N) && (node!=NODE_NE_MAIN)) { // Ignore room entry nodes
        d2Node = sqrt(pow(navNodes[node][0] - (eagle1.x*100),2) + pow(navNodes[node][1] - (eagle1.y*100),2));
        if(d2Node < dMin) {
          nodeNearHome = node;
        }
      }
    }
  }
  
  // ADD PATH TO HOME NODE FROM NEAREST NODE
  navPaths[NUM_PATHS-1][1] = nodeNearHome;
  
  Serial.print("SET HOME PATH: ");
  Serial.print(navPaths[NUM_PATHS-1][0]);
  Serial.print(" --> ");
  Serial.println(navPaths[NUM_PATHS-1][1]);
}

// IN ROOM CHECK LOGIC
bool inNWRoom() {
  return ((eagle1.x <= 0.70) && (eagle1.y >= 1.60));
}

bool inSWRoom() {
  return ((eagle1.x <= 0.70) && (eagle1.y <= 1.00));
}

bool inSERoom() {
  return ((eagle1.x >= 1.30) && (eagle1.y <= 0.90));
}

bool inNERoom() {
  return ((eagle1.x >= 1.30) && (eagle1.x <= 1.90) && (eagle1.y >= 1.50) && (eagle1.y <= 2.00));
}
