#include "Robot.h"

// ISR stuff need to outside
int encL_A;		// Left Encoder pin A
int encL_B;		// Left Encoder pin B
int encR_A;		// Right Encoder pin A
int encR_B;		// Right Encoder pin B
void doEncL_A();
void doEncL_B();
void doEncR_A();
void doEncR_B();
bool L_A_set;
bool L_B_set;
bool R_A_set;
bool R_B_set;
int encL_pos;
int encR_pos;

// CONSTRUCTOR
Robot::Robot(float _r, float _b, int _encL_A, int _encL_B, int _encR_A, int _encR_B, float _wMax) {
	R = _r;
	B = _b;
	encL_A = _encL_A;
	encL_B = _encL_B;
	encR_A = _encR_A;
	encR_B = _encR_B;
	lOut = 0;
	rOut = 0;
	Kp_v = 60;
	Ki_v = 120;
	Kd_v = 0;
	wMax = _wMax;
}

// INITIALIZE systems
void Robot::init() {
	Serial3.begin(38400);
	attachInterrupt(encL_A, doEncL_A, CHANGE);
	attachInterrupt(encL_B, doEncL_B, CHANGE);
	attachInterrupt(encR_A, doEncR_A, CHANGE);
	attachInterrupt(encR_B, doEncR_B, CHANGE);
	encL_pos = 0;
	encR_pos = 0;
	_thetaL = 0;
	_thetaR = 0;
	x = 0;
	y = 0;
	psi = 0;
	vMode = false;
	int_ev = 0;
	_ev = 0;
	_t_us = (float) micros();
}

// SETS x and y data (if not using relative)
void Robot::setPos(float _x, float _y) {
	x = _x;
	y = _y;
}

// SETS target velocity to PI controller
void Robot::setTargetVelocity(float _v) {
	v_TARGET = _v;
}

// SETS target heading
void Robot::setTargetHeading(float _psi) {
	psi_TARGET = _psi;
}

// SETS left and right motor speeds
void Robot::setMotorSpeeds(float l, float r) {
	if(l > 100) {
		l = 100;
	} else if(l < -100) {
		l = -100;
	}
	
	if(r > 100) {
		r = 100;
	} else if(r < -100) {
		r = -100;
	}
	int lCmd = (int) (64 - ((l*63)/100));
	int rCmd = (int) (192 + ((r*63)/100));
	Serial3.write(lCmd);
	Serial3.write(rCmd);
}

// ACCELERATES robot to specified motor speeds - to prevent it from tipping over
void Robot::accelToSpeed(float l_a, float r_a, float l, float r, float ms) {
	if(l > 100) {
		l = 100;
	} else if(l < -100) {
		l = -100;
	}
	
	if(r > 100) {
		r = 100;
	} else if(r < -100) {
		r = -100;
	}
	
	while(abs(l-lOut) > 0 || abs(r-rOut) > 0) {
		if(l > lOut + l_a) {
			lOut = lOut + l_a;
		} else if(l < lOut - l_a) {
			lOut = lOut - l_a;
		} else {
			lOut = l;
		}
		if(r > rOut + r_a) {
			rOut = rOut + r_a;
		} else if(r < rOut - r_a) {
			rOut = rOut - r_a;
		} else {
			rOut = r;
		}
		setMotorSpeeds(lOut, rOut);
		delay(ms);
	}
}

// STOPS both motors
void Robot::stopMotors() {
	vMode = false;
	setMotorSpeeds(0,0);
}

// RUNS the algorithims - must either be called every run or started on the scheduler
void Robot::run() {
	thetaL = ((float) encL_pos)*(PI/400.0);
	thetaR = ((float) encR_pos)*(PI/400.0);
	
	t_us = (float) micros();
	dt = (t_us - _t_us)/1000000.0; // delta time Microseconds to seconds
	
	thetaL_DOT = (thetaL-_thetaL)/dt;
	thetaR_DOT = (thetaR-_thetaR)/dt;
	
	v = (R/2)*(thetaR_DOT + thetaL_DOT); // Linear velocity
	w = -(R/(2*B))*(thetaR_DOT - thetaL_DOT); // Angular velocity
	
	psi += w*dt; // Integrate w for psi
	
	// Keep heading within 0 and 360 deg
	if(psi > 2*PI) {
		psi -= 2*PI;
	} else if(psi < 0) {
		psi += 2*PI;
	}
	
	x += v*dt*cos(psi);
	y += v*dt*sin(psi);
	
	_thetaL = thetaL;
	_thetaR = thetaR;
	_t_us = t_us;
	delay(5);
	
	if(vMode) { // Linear velocity control mode
		ev = v_TARGET - v;
		int_ev += ev*dt;
		// Saturate velocity error integral								   FIXME
		
		vCmd = (Kp_v*ev) + (Ki_v*int_ev) + (Kd_v*(ev-_ev)/dt);
		_ev = ev;
		
		epsi = psi_TARGET - psi;
		
		if(epsi > PI) {
			epsi -= 2*PI;
		} else if(epsi < -PI) {
			epsi += 2*PI;
		}
		
		if(epsi > PI/4) {
			wCmd = wMax;
		} else if(epsi < - PI/4) {
			wCmd = -wMax;
		} else {
			wCmd = (epsi*4*wMax)/PI;
		}
		
		accelToSpeed(4,4,vCmd+wCmd,vCmd-wCmd,15);
		// setMotorSpeeds(vCmd, vCmd);
	}
}

void Robot::debug(String msg) {
	Serial.print("[");
	Serial.print((int) (millis()/1000));
	Serial.print(".");
	int decimalVal = (int) (millis()%1000);
	if(decimalVal > 99) {
		Serial.print(decimalVal);
	} else if(decimalVal > 9) {
		Serial.print("0");
		Serial.print(decimalVal);
	} else {
		Serial.print("00");
		Serial.print(decimalVal);
	}
	Serial.print("s] ");
	Serial.println(msg);
}

// Encoder updates (ISR overwrites)
void doEncL_A() {
	if(digitalRead(encL_A) == HIGH) {
		L_A_set = true;
		if(!L_B_set) {
			encL_pos++;
		}
	} else {
		L_A_set = false;
	}
}

void doEncL_B() {
	if(digitalRead(encL_B) == HIGH) {
		L_B_set = true;
		if(!L_A_set) {
			encL_pos--;
		}
	} else {
		L_B_set = false;
	}
}

void doEncR_A() {
	if(digitalRead(encR_A) == HIGH) {
		R_A_set = true;
		if(!R_B_set) {
			encR_pos++;
		}
	} else {
		R_A_set = false;
	}
}

void doEncR_B() {
	if(digitalRead(encR_B) == HIGH) {
		R_B_set = true;
		if(!R_A_set) {
			encR_pos--;
		}
	} else {
		R_B_set = false;
	}
}
