#ifndef Robot_h
#define Robot_h

#define PI	3.14159

#include "Arduino.h"
#include <math.h>

class Robot {
	public:
		// PUBLIC FUNCTIONS
		
		// CONSTRUCTOR
		Robot(float _r, float _b, int _encL_A, int _encL_B, int _encR_A, int _encR_B, float _wMax);
		
		// INITIALIZE systems
		void init();
		
		// SETS x and y data (if not using relative)
		void setPos(float _x, float _y);
		
		// SETS target velocity to PI controller
		void setTargetVelocity(float _v);
		
		// SETS target heading
		void setTargetHeading(float _psi);
		
		// SETS left and right motor speeds
		void setMotorSpeeds(float l, float r);
		
		// ACCELERATES robot to specified motor speeds - to prevent it from tipping over
		void accelToSpeed(float l_a, float r_a, float l, float r, float ms);
		
		// STOPS both motors
		void stopMotors();
		
		// RUNS the algorithims - must either be called every run or started on the scheduler
		void run();
		
		// MEASURED DATA
		float x;		// x-position
		float y;		// y-position
		float psi;		// Heading
		float w;		// Angular Velocity
		float v;		// Linear Velocity
		bool vMode;		// Linear velocity control mode
		
		float Kp_v;		// Proportional gain for velocity controller
		float Ki_v;		// Integral gain for velocity controller
		float Kd_v;		// Derivative gain for velocity controller
	private:
		// INTERMEDIATE CALCULATION DATA
		float thetaL_DOT;	// Left wheel angular velocity
		float thetaR_DOT;	// Right wheel angular velocity
		float wMax;
		float wCmd;		// Command angular velocity
		float epsi;		// Error in heading
		float psi_TARGET;	// Target Heading
		float t_us;		// time
		float _t_us;	// Previous frame time
		float thetaL;
		float thetaR;
		float _thetaL;	// Previous theta L
		float _thetaR;	// Previous theta R
		float dt;		// Time step between runs
		float x_DOT;	// x-speed
		float y_DOT;	// y-speed
		float ev;		// Velocity error
		float _ev;		// Previous velocity error
		float R;		// Wheel radius
		float B;		// Wheel base radius
		float vCmd;		// Command velocity
		float v_TARGET;	// Target Linear Velocity
		float int_ev;	// Integral of error in velocity
		
		float int_ev_max;	// velocity error saturation limit
		float lOut;
		float rOut;
		void debug(String msg);
};

#endif // Robot_h
