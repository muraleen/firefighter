# README - iMx6 Side #

The firefighter program is the computation-intensive side of the Eagle1 fire fighting robot built by the Eagle Robotics Club of Embry-Riddle Aeronautical University, Prescott AZ. The program is meant to run on a linux system with GPIO and UART access to a second processor running the lower level control algorithms and interface to the motors, fire detection system and fire extinguishing system.

### Project Summary ###

* The program is meant for localization (for some parts, full-on SLAM) and navigation within a possibility of 4 possible arenas.
* The program interfaces with an RPLiDAR ([rplidar.robopeak.com](rplidar.robopeak.com)) by RoboPeak for localization.
* The program communicates with a second lower level processor using GPIOs and UARTs on board the processor.
* Designed to be used with an UDOO Quad Board where the second processor is the onboard SAM3X.

### Installation Instructions ###

* Install build dependencies: base-devel OR build-essential and gcc

Debian:
```
#!sh

apt-get install build-essential gcc
```
Archlinux:

```
#!sh

pacman -Sy base-devel gcc
```

* Get the project source code


```
#!sh

git clone https://muraleen@bitbucket.org/muraleen/firefighter.git firefighter
```

* Compile the program


```
#!sh

cd firefighter
./compile.sh
```

If the program fails to run the compile script, it may not have the correct permissions, run:

```
#!sh

chmod +x compile.sh
./compile.sh
```

Or if that doesn't work either, try:


```
#!sh

sh compile.sh
```

**NOTE: ** If you've already compiled the program before and only wish the recompile the firefighter program without touching the RPLiDAR SDK, just run make instead of ./compile.sh

* Run the program


```
#!sh

./bin/firefighter
```

**NOTE: ** If you'd like the program to run on start-up (we needed that as the program helps achieve the primary objective of the robot), copy the *ffboot* script from the scripts directory and add the script to the system's start-up service list.

```
#!sh

cp scripts/ffboot /etc/init.d/
chmod 755 /etc/init.d/ffboot
update-rc.d ffboot defaults
```

You can remove the program from the start-up service list by running the following command.

```
#!sh

update-rc.d -f ffboot remove
```

### Project Update Instructions ###

You can run the included update script to automatically pull the changes made to your installation. Ofcourse, you'd need to recompile the program after the update.

```
#!sh

./update.sh
make
```

# README - SAM3X Side #

This is pretty straight forward, use the arduino sketch with the included Robot library in Arduino IDE 1.5.4 or 1.6.0 [RECOMMENDED].

### Contribution Guidelines ###

* Please make a fork or clone of this project to work on your own changes; I will allow bug fixes to be merged into the master branch, but all major changes should not affect our eagle1 project.
* If you would like any help porting these algorithms into different boards or projects, I will gladly do my best to help, please contact me using my email address below.

### Project Repository Owner ###

* Narendran Muraleedharan ([muraleen@my.erau.edu](emailto:muraleen@my.erau.edu))
